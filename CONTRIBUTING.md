# Install for dev environment

## Prerequisites

| Stack      |
| ---------- |
| PHP7.1+    |
| MySQL      |
| composer   |
| Node.js    |

## Fork the project

## Configuration

Copy `./.env.sample` and put on it the good parameters and past it like `./.env`

## Install libs and bundles

From the project root launch:

-   `composer install` in order to install composer libs and bundles.
-   `npm install` in order to install npm libs.
-   Create symlink from `./public/` directory to `./node_modules/`.

## Create schema and run migrations

From the project root, run the following commands:

```shell
php bin/console doctrine:schema:create
```

## Server

This web app needs a server. You could use any server like Nginx, Apache, [PHP server](https://www.php.net/manual/fr/features.commandline.webserver.php) or [Symfony server](https://symfony.com/doc/current/setup/symfony_server.html).

NB: The public HTML must be the public root (`/path/to/odl/public`).

## Merge requests

Please, use a clear and explicit title for your merge request. Same for the MR description.

To the development appears on the changelog, write the title like that:

`feat: my feature title`

`improve: my improvement title`

`fix: my fix title`

See the changelog config for more.

NB: For commit's name, use a clear and sample name or squash it.

## php-cs-fixer

Please, install [php-cs-fixer](https://cs.symfony.com/) in globally to respect PSR2.

```
composer global require friendsofphp/php-cs-fixer
export PATH="$PATH:$HOME/.composer/vendor/bin"
```

For all your MR, run this from the root project:
```
php-cs-fixer fix --config .php_cs.dist
```

# Changelog

Use [git-chglog](https://github.com/git-chglog/git-chglog).
[Changelog](CHANGELOG.md) is generated using `chglog` and `next-tag` feature :

```shell
git-chglog --next-tag mytag > CHANGELOG.md
```

Config is [here](./chglog)
