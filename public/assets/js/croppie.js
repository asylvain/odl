$(document).ready(function() {
  var croppieContainer = document.querySelector('#avatar-croppie');
  var userId = croppieContainer.dataset.userId;
  var $uploadCrop;

  function readFile(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('.upload-avatar').addClass('ready');
        $uploadCrop
          .croppie('bind', {
            url: e.target.result
          })
          .then(function() {
            console.log('jQuery bind complete');
          });
      };

      reader.readAsDataURL(input.files[0]);
    } else {
      swal("Sorry - you're browser doesn't support the FileReader API");
    }
  }

  $uploadCrop = $(croppieContainer).croppie({
    enableExif: true,
    viewport: {
      width: 100,
      height: 100,
      type: 'circle'
    },
    boundary: {
      width: 200,
      height: 200
    }
  });

  $('#user_data_form_avatar_upload').on('change', function() {
    readFile(this);
  });

  $('.upload-result').on('click', function(ev) {
    $uploadCrop
      .croppie('result', {
        type: 'canvas',
        size: 'viewport'
      })
      .then(function(resp) {
        popupResult({
          src: resp
        });
      });
  });

  $('.upload-result').on('click', function(ev) {
    $uploadCrop
      .croppie('result', {
        type: 'canvas',
        size: 'viewport'
      })
      .then(function(resp) {
        popupResult({
          src: resp
        });
      });
  });

  $('.upload-valid').on('click', function(ev) {
    $uploadCrop
      .croppie('result', {
        type: 'canvas',
        size: 'viewport'
      })
      .then(function(resp) {
        $('#user_data_form_avatar_base64').val(resp);
        swal('Your avatar will be take into account.');
      });
  });

  $('.user-avatar .close.icon').on('click', function() {
    swal({
      text: 'Do you really want to remove your avatar?',
      buttons: {
        cancel: {
          text: 'Cancel',
          value: false,
          visible: true,
          className: '',
          closeModal: true
        },
        confirm: {
          text: 'OK',
          value: true,
          visible: true,
          className: '',
          closeModal: true
        }
      }
    }).then(function(response) {
      if (response) {
        $.ajax({
          method: 'DELETE',
          data: {
            id: userId
          },
          url: Routing.generate('odl_ajax_user_avatar_remove'),
          statusCode: {
            200: function() {
              $('.user-avatar').remove();
            }
          }
        });
      }
    });
  });

  function popupResult(result) {
    swal({
      title: '',
      content: {
        element: 'img',
        attributes: {
          src: result.src
        }
      },
      allowOutsideClick: true
    });
    setTimeout(function() {
      $('.sweet-alert').css('margin', function() {
        var top = -1 * ($(this).height() / 2),
          left = -1 * ($(this).width() / 2);

        return top + 'px 0 0 ' + left + 'px';
      });
    }, 1);
  }
});
