$(document).ready(function() {
  /**
   * Variables for Toggle content_period and Pagination
   */
  var styles = {
    color: 'red',
    border: 'solid 1px grey',
    borderRadius: '10px'
  };
  var NoStyles = {
    color: 'lightgrey',
    border: 'none'
  };

  /**
   * Toggle content_period
   */
  $('.title_period').click(function() {
    var classThis = $(this).attr('class');
    var period = classThis.replace('title_period btn_', '');

    $('div.content_period#' + period).toggle();
    if ($('div.content_period#' + period).css('display') == 'block') {
      getCovers($(this), 'page_1');
      $('div.content_period#' + period)
        .children('table')
        .css('display', 'none');
      $('div.content_period#' + period)
        .children('table.page_1')
        .toggle();
      $('div.content_period').css('display', 'none');
      $('div.content_period#' + period).toggle();
    }

    $('div.content_period#' + period)
      .children('div')
      .children('.btn_page')
      .css(NoStyles);
    $('div.content_period#' + period)
      .children('div')
      .children('button.btn_page_1')
      .css(styles);
    $('div.content_period#' + period)
      .children('div')
      .children('.btn_prev')
      .css('display', 'none');
    $('div.content_period#' + period)
      .children('div')
      .children('.btn_next')
      .css('display', 'block');
  });

  /**
   * Pagination
   */
  $('.btn_next').click(function() {
    var id = $(this)
      .parent('div')
      .parent('div')
      .attr('id');
    var length = $('div.content_period#' + id)
      .children('div')
      .children('.btn_pagination.top > button').length;
    length = length - 2;
    for (a = 1; a <= length; a++) {
      var search =
        $('div.content_period#' + id)
          .children('div')
          .children('.btn_page_' + a)
          .css('color') == 'rgb(255, 0, 0)';
      if (search === true) {
        var Hp = a;
        var Sp = a + 1;
        break;
      }
    }
    var ShowPage = 'page_' + Sp;
    var HidePage = 'page_' + Hp;
    getCovers($(this), ShowPage);
    $('div.content_period#' + id)
      .children('.' + ShowPage)
      .toggle();
    $('div.content_period#' + id)
      .children('.' + HidePage)
      .toggle();
    $('html,body').animate(
      {
        scrollTop: $('h2.btn_' + id).offset().top
      },
      'slow'
    );
    $('div.content_period#' + id)
      .children('div')
      .children('.btn_prev')
      .css('display', 'block');
    $('div.content_period#' + id)
      .children('div')
      .children('.btn_page')
      .css(NoStyles);
    $('div.content_period#' + id)
      .children('div')
      .children('.btn_page_' + Sp)
      .css(styles);
    if (Sp == length) {
      $('div.content_period#' + id)
        .children('div')
        .children('.btn_next')
        .css('display', 'none');
    }
  });

  $('.btn_prev').click(function() {
    var id = $(this)
      .parent('div')
      .parent('div')
      .attr('id');
    var length = $('div.content_period#' + id)
      .children('div')
      .children('.btn_pagination.top > button').length;
    length = length - 2;
    for (a = 1; a <= length; a++) {
      var search =
        $('div.content_period#' + id)
          .children('div')
          .children('.btn_page_' + a)
          .css('color') == 'rgb(255, 0, 0)';
      if (search === true) {
        var Hp = a;
        var Sp = a - 1;
        break;
      }
    }

    var ShowPage = 'page_' + Sp;
    var HidePage = 'page_' + Hp;
    getCovers($(this), ShowPage);
    $('div.content_period#' + id)
      .children('.' + ShowPage)
      .toggle();
    $('div.content_period#' + id)
      .children('.' + HidePage)
      .toggle();
    $('html,body').animate(
      {
        scrollTop: $('h2.btn_' + id).offset().top
      },
      'slow'
    );
    $('div.content_period#' + id)
      .children('div')
      .children('.btn_next')
      .css('display', 'block');
    $('div.content_period#' + id)
      .children('div')
      .children('.btn_page')
      .css(NoStyles);
    $('div.content_period#' + id)
      .children('div')
      .children('.btn_page_' + Sp)
      .css(styles);
    if (Sp == 1) {
      $('div.content_period#' + id)
        .children('div')
        .children('.btn_prev')
        .css('display', 'none');
    }
  });

  $('.btn_page').click(function() {
    var nbrP = $(this)
      .parent('.btn_pagination')
      .children('button').length;
    nbrP = nbrP - 2; // Remove 'next' and 'previous' buttons from the count
    var Sp = $(this).html();
    var ShowPage = 'page_' + Sp;
    var id = $(this)
      .parent('div')
      .parent('div')
      .attr('id');
    getCovers($(this), ShowPage);
    for (a = 1; a <= nbrP; a++) {
      var search =
        $('div.content_period#' + id)
          .children('table.page_' + a)
          .css('display') == 'table';
      if (search === true) {
        var HidePage = 'page_' + a;
        break;
      }
    }
    $('div.content_period#' + id)
      .children('.' + HidePage)
      .toggle();
    $('div.content_period#' + id)
      .children('.' + ShowPage)
      .toggle();
    $('html,body').animate(
      {
        scrollTop: $('h2.btn_' + id).offset().top
      },
      'slow'
    );
    var styles = {
      color: 'red',
      border: 'solid 1px grey',
      borderRadius: '10px'
    };
    var NoStyles = {
      color: 'lightgrey',
      border: 'none'
    };
    $('div.content_period#' + id)
      .children('div')
      .children('.btn_page')
      .css(NoStyles);
    $('div.content_period#' + id)
      .children('div')
      .children('button.btn_' + ShowPage)
      .css(styles);
    $('div.content_period#' + id)
      .children('div')
      .children('.btn_prev')
      .css('display', 'block');
    $('div.content_period#' + id)
      .children('div')
      .children('.btn_next')
      .css('display', 'block');
    if (ShowPage == 'page_1') {
      $('div.content_period#' + id)
        .children('div')
        .children('.btn_prev')
        .css('display', 'none');
    }
    if (ShowPage == 'page_' + nbrP) {
      $('div.content_period#' + id)
        .children('div')
        .children('.btn_next')
        .css('display', 'none');
    }
  });
});

function getCovers(object, page) {
  var period = object.parents('div.period').attr('id');
  var lines = $('div.period#' + period).find('table.' + page + ' tr');
  var ids = [];

  lines.each(function() {
    ids.push($(this).attr('id'));
  });

  $.ajax({
    method: 'POST',
    url: Routing.generate('odl_ajax_arc_get_covers'),
    data: {
      ids: ids
    },
    success: function(ret) {
      var i = 0;
      lines.each(function() {
        if (i < 20) {
          $(this)
            .find('td.cel_img img')
            .prop('src', '/' + ret[i]);
          i++;
        } else {
          return false;
        }
      });
    }
  });
}
