$(document).ready(function() {
  $('.title_universe').click(function() {
    var classThis = $(this).attr('class');
    var tmp = new RegExp(/btn_([a-z0-9]+)/, 'i');
    var id_universe = classThis.match(tmp)[1];
    $('#' + id_universe).toggle();
  });

  /**
   * Checkbox behaviour
   */
  $('#CBisUrban').click(function() {
    var isChecked = $('#CBisUrban').prop('checked');
    if (isChecked === true) {
      $('#LinkUrban').css('display', 'block');
      $('#LinkUrban').prop('required', true);
    } else {
      $('#LinkUrban').css('display', 'none');
      $('#LinkUrban').attr('value', '');
      $('#LinkUrban').prop('required', false);
    }
  });

  $('#CBisDCT').click(function() {
    var isChecked = $('#CBisDCT').prop('checked');
    if (isChecked === true) {
      $('#LinkDCT').css('display', 'block');
      $('#LinkDCT').prop('required', true);
    } else {
      $('#LinkDCT').css('display', 'none');
      $('#LinkDCT').attr('value', '');
      $('#LinkDCT').prop('required', false);
    }
  });

  $('#selectCreate').click(function() {
    var ChoiceCreate = $('.optionCreate:selected').attr('value');
    if (ChoiceCreate === 'universe') {
      $('#create_universe').css('display', 'block');
      $('#create_era').css('display', 'none');
      $('#create_period').css('display', 'none');
      $('.btn_send').css('display', 'block');
    } else if (ChoiceCreate === 'era') {
      $('#create_universe').css('display', 'none');
      $('#create_era').css('display', 'block');
      $('#create_period').css('display', 'none');
      $('.btn_send').css('display', 'block');
    } else if (ChoiceCreate === 'period') {
      $('#create_universe').css('display', 'none');
      $('#create_era').css('display', 'none');
      $('#create_period').css('display', 'block');
      $('.btn_send').css('display', 'block');
    } else if (ChoiceCreate === '') {
      $('#create_universe').css('display', 'none');
      $('#create_era').css('display', 'none');
      $('#create_period').css('display', 'none');
      $('.btn_send').css('display', 'none');
    }
  });

  $('#whichUniverseForEra').click(function() {
    var universeSelected = $('.selectUniverseForEra:selected').attr('value');
    var selectEra;

    if (universeSelected !== '' && universeSelected !== undefined) {
      $.ajax({
        method: 'GET',
        url: '/ajax/fetch-section.php?type=universe&id=' + universeSelected,
        success: function(data) {
          selectEra = '<option value="" selected>Cliquez</option>\n';
          selectEra += '<option value="first">En premier</option>\n';
          $(data).each(function(i) {
            selectEra += '<option value="' + data[i].id + '">Après ' + data[i].name + '</option>\n';
          });
          $('#selectEraForEra').toggle();
          $('#whereEra').html(selectEra);
        }
      });
    }
  });

  $('#whichUniverseForPeriod').click(function() {
    var universeSelected = $('.selectUniverseForPeriod:selected').attr('value');
    var selectEra;

    if (universeSelected !== '' && universeSelected !== undefined) {
      $.ajax({
        method: 'GET',
        url: '/ajax/fetch-section.php?type=universe&id=' + universeSelected,
        success: function(data) {
          selectEra = '<option value="" selected>Cliquez</option>\n';
          $(data).each(function(i) {
            selectEra += '<option class="selectEra" value="' + data[i].id + '">' + data[i].name + '</option>\n';
          });
          $('#selectEraForPeriod').toggle();
          $('#whichEra').html(selectEra);
        }
      });
    }
  });

  $('#whichEra').click(function() {
    var eraSelected = $('.selectEra:selected').attr('value');
    var selectPeriod;

    if (eraSelected !== '' && eraSelected !== undefined) {
      $.ajax({
        method: 'GET',
        url: '/ajax/fetch-section.php?type=era&id=' + eraSelected,
        success: function(data) {
          selectPeriod = '<option value="first">En premier</option>\n';
          $(data).each(function(i) {
            selectPeriod += '<option value="' + data[i].id + '">Après ' + data[i].name + '</option>\n';
          });
          $('#selectPeriod').toggle();
          $('#wherePeriod').html(selectPeriod);
        }
      });
    }
  });

  /**
   * Back to top button
   */
  $('#btnup').click(function() {
    $('html,body').animate(
      {
        scrollTop: $('body').offset().top - 42 // 42 is the margin-top for the menu
      },
      'slow'
    );
  });

  /**
   * Button close on bottom of content_period
   */
  $('.btn_hide.down').click(function() {
    var div = $(this)
      .parent('div.content_period')
      .attr('id');
    $('div.content_period#' + div).toggle();
  });
});

function nl2br(str) {
  return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + '<br />' + '$2');
}

function checkUpdateEqual(that, ref) {
  if (that == ref) return undefined;
  else return that;
}

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $('td.cel_img img').attr('src', e.target.result);
    };
    reader.readAsDataURL(input.files[0]);
  }
}

function resetInput(e) {
  e.wrap('<form>')
    .closest('form')
    .get(0)
    .reset();
  e.unwrap();
}

function fillSelect(name_selected, name_select_to_add) {
  var universe_selected = $('select[name="' + name_selected + '"] > option:selected').attr('value');
  var options;

  if (universe_selected !== '' && universe_selected !== undefined) {
    $.ajax({
      method: 'GET',
      async: false,
      url: '/ajax/fetch-section.php?type=' + name_selected + '&id=' + universe_selected,
      success: function(data) {
        options = '<option value="" selected>' + name_select_to_add + '</option>\n';
        n = data.length;
        isSelected = '';
        $(data).each(function(i) {
          if (i == n - 1) {
            isSelected = 'selected';
          }
          options += '<option value="' + data[i].id + '" ' + isSelected + '>' + data[i].name + '</option>\n';
        });
        $('select[name="' + name_select_to_add + '"]').html(options);
      }
    });
  }
}

function getUrlParameter(sParam) {
  var sPageURL = window.location.search.substring(1),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;

  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');

    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
    }
  }
}

function addWaiter() {
  var windowHeight = $(window).height();
  var top = $(window).scrollTop();
  $('<div/>', {
    id: 'waiting',
    class: 'ui segment',
    style: 'position: absolute; top: ' + top + 'px; left: 0; height: ' + windowHeight + 'px; width: 100%;'
  }).appendTo('section.ui.middle.aligned.center.aligned.grid.main-section');
  $('<div/>', {
    id: 'waiting-dimmer',
    class: 'ui active dimmer'
  }).appendTo('#waiting');
  $('<div/>', {
    id: 'waiting-loader',
    class: 'ui text loader',
    text: 'Loading, please wait!'
  }).appendTo('#waiting-dimmer');
}
