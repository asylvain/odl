$(document).ready(function () {
  var categoryContent = [];
  const searchBar = document.getElementById('search-bar');
  const resultsContainer = document.getElementById('search-results');

  if (typeof eraId !== 'undefined') {
    var from = eraId;
  } else {
    var from = 'all';
  }

  // For reading order pages
  if (typeof periods !== 'undefined') {
    for (key in periods) {
      var period = periods[key];
      $.ajax({
        method: 'GET',
        url: Routing.generate('odl_ajax_arcsByPeriod'),
        data: {
          id: period['id'],
        },
        async: false,
        success: function (arcs) {
          for (key in arcs) {
            var content = {
              category: period['name'],
              id: arcs[key]['id'],
              image: pathAssetCover + arcs[key]['cover'],
              title: arcs[key]['title'],
              description: arcs[key]['content'],
            };
            categoryContent.push(content);
          }
        },
      });
    }

    searchBar.addEventListener('keyup', function (e) {
      resultsContainer.innerHTML = '';
      const term = e.target.value.toLowerCase();
      var category = '';
      var loop = 0;

      if (term) {
        categoryContent.forEach(function (arc) {
          if (loop > 2) {
            return;
          } else {
            if (arc.title.toLowerCase().indexOf(term) != -1 || arc.description.toLowerCase().indexOf(term) != -1) {
              loop++;
              var html =
                '<div class="image">' +
                '<img src="' +
                arc.image +
                '">' +
                '</div>' +
                '<div class="content">' +
                '<div class="title">' +
                arc.title +
                '</div>' +
                '<div class="description">' +
                arc.description +
                '</div>' +
                '</div>';
              var template = document.createElement('a');
              template.className = 'result';
              template.dataset.id = arc.id;
              template.addEventListener('click', clickOnResult);
              template.innerHTML = html.trim();
              if (category != arc.category) {
                category = arc.category;
                var categoryTemplate = document.createElement('div');
                categoryTemplate.className = 'category';
                var categoryNameTemplate = document.createElement('div');
                categoryNameTemplate.className = 'name';
                categoryNameTemplate.innerHTML = arc.category;
                var resultsTemplate = document.createElement('div');
                resultsTemplate.className = 'results';
                categoryTemplate.appendChild(categoryNameTemplate);
                categoryTemplate.appendChild(resultsTemplate);
              } else {
                var categoriesTemplate = resultsContainer.getElementsByClassName('category');
                for (var i = 0; i < categoriesTemplate.length; i++) {
                  var categoryTemplate = categoriesTemplate[i];
                  if (categoryTemplate.getElementsByClassName('name')[0].innerHTML == arc.category) {
                    break;
                  }
                }
              }
              categoryTemplate.getElementsByClassName('results')[0].appendChild(template);
              resultsContainer.appendChild(categoryTemplate);
            }
          }
        });
        var viewMore = document.createElement('a');
        viewMore.className = 'ui bottom attached button';
        viewMore.innerText = 'View more...';
        encodedTerm = encodeURI(term);
        viewMore.href = Routing.generate('odl_search_arcs') + '?from=' + from + '&q=' + encodedTerm;
        resultsContainer.appendChild(viewMore);
        resultsContainer.classList.add('transition', 'visible');
        resultsContainer.style.display = 'block';
      }
    });
  }

  searchBar.addEventListener('keyup', function (e) {
    // 'Enter' key
    if (e.keyCode === 13) {
      const term = e.target.value.toLowerCase();
      encodedTerm = encodeURI(term);
      window.location = Routing.generate('odl_search_arcs') + '?from=' + from + '&q=' + encodedTerm;
    }
  });

  searchBar.onblur = function () {
    setTimeout(function () {
      resultsContainer.style.display = 'none';
      resultsContainer.classList.remove('transition', 'visible');
    }, 100);
  };

  searchBar.onfocus = function () {
    if (resultsContainer.getElementsByClassName('results').length) {
      resultsContainer.classList.add('transition', 'visible');
      resultsContainer.style.display = 'block';
    }
  };

  function clickOnResult(e) {
    id = e.target.parentNode.parentNode.dataset.id;

    $.ajax({
      method: 'GET',
      url: Routing.generate('odl_search_arc') + '/' + id,
      success: function (response) {
        var html = response.html;
        var blackFilter = document.createElement('div');
        blackFilter.className = 'ui active dimmer search';
        blackFilter.setAttribute('id', 'result-arc');
        blackFilter.style.cssText = 'top: ' + document.documentElement.scrollTop + 'px !important';
        blackFilter.style.position = 'absolute';
        blackFilter.style.left = 0;
        blackFilter.style.height = window.innerHeight + 'px';
        blackFilter.style.width = '100%';
        var title = document.createElement('div');
        title.className = 'ui message';
        title.innerText = response.title;
        var button = document.createElement('button');
        button.addEventListener('click', closeResult);
        button.className = 'ui button';
        button.style.position = 'absolute';
        button.style.top = '5px';
        button.style.right = '5px';
        button.innerHTML = '<i class="close icon"></i>';
        blackFilter.innerHTML = html.trim();
        blackFilter.appendChild(title);
        blackFilter.appendChild(button);
        document.getElementsByTagName('body')[0].appendChild(blackFilter);
      },
    });
  }

  function closeResult(e) {
    document.getElementById('result-arc').remove();
  }
});
