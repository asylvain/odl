$(document).ready(function() {
  var dataFromTwig = document.querySelector('#twig-to-js');
  var entityType = dataFromTwig.dataset.entityTypeClean;
  var childEntity = dataFromTwig.dataset.childEntity;
  var pathAssetsBanner = dataFromTwig.dataset.pathAssetsBanner;
  var pathAssetsLogos = dataFromTwig.dataset.pathAssetsLogos;
  var ajaxDeletePathName = 'odl_ajax_' + entityType + '_delete_' + entityType;
  // Store the div that contains the 'data-prototype'
  var container = $('div#' + entityType + 's_form_' + entityType + 's');

  // Define a count to name fields will be dynamically added
  var index = container.find(':input').length;

  // Add fields for new entity when the button 'add' is clicked
  $('#add_entity').click(function(e) {
    addEntity(container);

    e.preventDefault();
    return false;
  });

  // Add fields for a first entity if there no exists
  if (index == 0) {
    addEntity(container);
  } else {
    // Add additionals at the existing entities
    var re = new RegExp(entityType + 's_form_' + entityType + 's_[\\w\\d]+');
    container.children('div').each(function() {
      var attrId = $(this).children('div').attr('id');
      if (re.test(attrId)) {
          var id = $(this).find('#' + attrId + '_id').val();
          if (entityType == 'era') {
              addImages($(this), id)
          }
          addLinks($(this), id);
      }
    });
  }

  // Function to add entity
  function addEntity(container) {
      var template = container.attr('data-prototype').replace(/__name__/g, index);

      var prototype = $(template);
      prototype.addClass('field formEntity');
      addLinks(prototype);
      container.append(prototype);

      index++;
  }

  function addImages(object, id) {
    $.ajax({
        method: 'GET',
        url: Routing.generate('odl_ajax_era_get_era'),
        data: {id: id},
        success: function(entity) {
            if (entity.image !== undefined && entity.image != '') {
                var image = pathAssetsBanner + entity.image;
                object.find('[id$="image"]').after('<img src="' + image + '">');
              }
              if (entity.logoLinkA !== undefined && entity.logoLinkA != '') {
                var logoLinkA = pathAssetsLogos + entity.logoLinkA;
                object.find('[id$="_logoLinkA"]').after('<img src="' + logoLinkA + '">');
              }
              if (entity.logoLinkB !== undefined && entity.logoLinkB != '') {
                var logoLinkB = pathAssetsLogos + entity.logoLinkB;
                object.find('[id$="_logoLinkB"]').after('<img src="' + logoLinkB + '">');
              }
        }
    })
  }


  // Function to add 'delete' ad 'edit' buttons
  function addLinks(prototype, id = '') {
    var containerButtons = $('<div class="ui buttons" id="' + id + '"></div>');
    var deleteLink = $('<button type="button" class="ui red button remove_entity">Supprimer</button>');

    if (childEntity != '') {
      var childEntityLink = $('<a href=""><button type="button" class="ui button edit_childentity">Éditer</button></a>');

      childEntityHref = Routing.generate('odl_admin_manage_' + childEntity, { id: id });
      childEntityLink.attr('href', childEntityHref);

      containerButtons.append(deleteLink, childEntityLink);
      prototype.append(containerButtons);
    } else {
      prototype.append(deleteLink);
    }

    deleteLink.click(function(e) {
      if (id != '') {
        var answer = confirm("Êtes-vous certain de vouloir supprimer cette section ? Ceci supprimera également tout ce qu'elle contient de manière irréversible !");

        if (answer === true) {
          $.ajax({
            type: 'DELETE',
            url: Routing.generate(ajaxDeletePathName),
            data: {
              id: id
            },
            success: function() {
              var formContainer = $('.mainContainerForm');
              var message = 'Supprimé avec succès.';
              var messageFlash = formContainer.find('.ui.info.message');

              if (messageFlash.length > 0) {
                messageFlash.children('p').text(message);
              } else {
                messageFlash = $('<div class="ui info success message"><p>' + message + '</p></div>');
                $('.containerHeader').after(messageFlash);
              }
              prototype.remove();
              scrollTop();
            }
          });
        }
      } else {
        prototype.remove();
      }

      e.preventDefault();
      return false;
    });
  }
});
