$(document).ready(function() {
  $('.update_tr').click(function() {
    $('tr#updateInLine').remove();
    var line = $(this).parents('tr');
    var id = line.attr('id');
    var position = $('tr#' + id + ' .cel_id span').text();

    $.ajax({
      type: 'GET',
      url: Routing.generate('odl_ajax_arc_update', { id: id }),
      async: false,
      success: function(response) {
        var form = $(response);
        form.find('.arc_form_cover_label').html('<i class="icon upload"></i>' + form.find('.arc_form_cover_label').text());
        $('tr#' + id).after(form);
        $('tr#updateInLine').show('slow');
        $('#form_arc_update').submit(function(e) {
          e.preventDefault();
          var form = this;
          $.ajax({
            url: Routing.generate('odl_ajax_arc_update', { id: id }),
            data: new FormData(form), // the formData function is available in almost all new browsers.
            type: 'POST',
            contentType: false,
            processData: false,
            cache: false,
            dataType: 'json', // Change this according to your response from the server.
            success: function(data) {
              $('tr#updateInLine')
                .children('td.ui.form')
                .addClass('loading');
              var arc = data.arc;
              console.log(data);
              console.log(arc.position);
              if (arc.position != parseInt(position)) {
                location.reload();
              }

              // Display success message
              var popup = $('<div class="ui info ' + data.type + ' message"><p>' + data.message + '</p></div>');
              $('#message-flash-popup').html(popup);
              setTimeout(function() {
                $('#message-flash-popup').html('');
              }, 5000);

              // Update data on front-end
              //   var coversDirectory = $.ajax({
              //     type: 'GET',
              //     data: {
              //       path: 'assets/img/covers',
              //       packageName: arc.cover
              //     },
              //     url: Routing.generate('odl_ajax_covers_directory'),
              //     async: false
              //   }).responseText;
              //   console.log(coversDirectory);
              line.find('.cel_img img').attr('src', '/assets/img/covers/' + arc.cover);
              line.find('.cel_title h3').text(arc.title);
              line.find('.cel_content p').html(nl2br(arc.content));
              if (arc.isEvent) {
                line.addClass('isEvent');
              } else {
                line.removeClass('isEvent');
              }
              $('tr#updateInLine')
                .children('td.ui.form')
                .removeClass('loading');
            }
          });
        });
      }
    });

    $('#update_close').click(function() {
      $('tr#updateInLine').remove();
    });
    $('#fake-input').click(function() {
      $('#arc_form_cover').click();
    });
    fileInput = document.querySelector('#arc_form_cover');
    fileInput.addEventListener('change', function(event) {
      var text = $(this)
        .val()
        .replace('C:\\fakepath\\', '');
      $('#result-file-selected').text(text);
    });
  });

  /**
   * Trash button
   */
  $('.btn_trash').click(function() {
    addWaiter();
    var line = $(this).parents('tr');
    var tables = line.parents('div.content_period').find('table');
    var countElement = line
      .parents('div.content_period')
      .siblings('.arc-count-container')
      .children('span');
    var count = parseInt(countElement.text());
    var id = line.attr('id');
    var position = $('tr#' + id + ' .cel_id span').text();
    var data = {
      id: id
    };
    var answer = confirm("Voulez-vous vraiment supprimer l'arc " + position + ' ?');
    if (answer === true) {
      $.ajax({
        method: 'DELETE',
        data: data,
        url: Routing.generate('odl_ajax_arc_remove'),
        statusCode: {
          200: function() {
            // Remove arc
            $('tr#' + id)
              .parent('tbody')
              .parent('table')
              .remove();

            // Display success message
            popup = $('<div class="ui info success message"><p>Supprimé avec succès.</p></div>');
            $('#message-flash-popup').html(popup);
            setTimeout(function() {
              $('#message-flash-popup').html('');
            }, 5000);

            // Readjust all position
            tables.each(function() {
              var eachTr = $(this).find('tr');
              var eachId = eachTr.attr('id');
              var eachPositionSpan = $('tr#' + eachId + ' .cel_id span');
              var eachPosition = parseInt(eachPositionSpan.text());
              if (eachPosition > position) {
                var newPosition = (--eachPosition).toString();
                eachPositionSpan.text(newPosition);
              }
            });

            // Readjust the total count
            newCount = (--count).toString();
            countElement.text(newCount);
          }
        }
      });
    }
    // Remove waiter
    $('#waiting')
      .fadeOut('slow')
      .promise()
      .done(function() {
        $('#waiting').remove();
      });
  });
});
