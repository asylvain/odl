<?php

namespace ODL\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class Periods
{
    protected $periods;

    public function __construct()
    {
        $this->periods = new ArrayCollection();
    }

    public function getPeriods()
    {
        return $this->periods;
    }
}
