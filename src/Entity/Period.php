<?php

namespace ODL\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * @ORM\Entity(repositoryClass="ODL\Repository\PeriodRepository")
 */
class Period implements JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=13)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="ODL\Core\RandomIdGenerator")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="ODL\Entity\Era", inversedBy="periods")
     * @ORM\JoinColumn(nullable=false)
     */
    private $era;

    /**
     * @ORM\Column(type="integer")
     */
    private $position;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $cleanName;

    /**
     * @ORM\OneToMany(targetEntity="ODL\Entity\Arc", mappedBy="period", cascade={"remove"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $arcs;

    public function __construct()
    {
        $this->arcs = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getIdEra(): ?era
    {
        return $this->era;
    }

    public function setIdEra(?era $era): self
    {
        $this->era = $era;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCleanName(): ?string
    {
        return $this->cleanName;
    }

    public function setCleanName(string $cleanName): self
    {
        $this->cleanName = $cleanName;

        return $this;
    }

    /**
     * @return Collection|Arc[]
     */
    public function getArcs(): Collection
    {
        return $this->arcs;
    }

    public function addArc(Arc $arc): self
    {
        if (!$this->arcs->contains($arc)) {
            $this->arcs[] = $arc;
            $arc->setIdPeriod($this);
        }

        return $this;
    }

    public function removeArc(Arc $arc): self
    {
        if ($this->arcs->contains($arc)) {
            $this->arcs->removeElement($arc);
            // set the owning side to null (unless already changed)
            if ($arc->getIdPeriod() === $this) {
                $arc->setIdPeriod(null);
            }
        }

        return $this;
    }

    public function getEra(): ?Era
    {
        return $this->era;
    }

    public function setEra(?Era $era): self
    {
        $this->era = $era;

        return $this;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'era' => $this->era,
            'position' => $this->position,
            'name' => $this->name,
            'cleanName' => $this->cleanName,
            'arcs' => $this->arcs,
        ];
    }
}
