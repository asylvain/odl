<?php

namespace ODL\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class Eras
{
    protected $eras;

    public function __construct()
    {
        $this->eras = new ArrayCollection();
    }

    public function getEras()
    {
        return $this->eras;
    }
}
