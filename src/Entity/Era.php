<?php

namespace ODL\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * @ORM\Entity(repositoryClass="ODL\Repository\EraRepository")
 */
class Era implements JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=13)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="ODL\Core\RandomIdGenerator")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="ODL\Entity\Universe", inversedBy="eras")
     * @ORM\JoinColumn(nullable=false)
     */
    private $universe;

    /**
     * @ORM\Column(type="integer")
     */
    private $position;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $cleanName;

    /**
     * @ORM\Column(type="string", length=37)
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity="ODL\Entity\Period", mappedBy="era", cascade={"remove"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $periods;

    /**
     * @ORM\Column(type="string", length=37, options={"default": ""})
     */
    private $logoLinkA = '';

    /**
     * @ORM\Column(type="string", length=37, options={"default": ""})
     */
    private $logoLinkB = '';

    public function __construct()
    {
        $this->periods = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getIdUniverse(): string
    {
        return $this->universe->getId();
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCleanName(): ?string
    {
        return $this->cleanName;
    }

    public function setCleanName(string $cleanName): self
    {
        $this->cleanName = $cleanName;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|Period[]
     */
    public function getPeriods(): Collection
    {
        return $this->periods;
    }

    public function addPeriod(Period $period): self
    {
        if (!$this->periods->contains($period)) {
            $this->periods[] = $period;
            $period->setIdEra($this);
        }

        return $this;
    }

    public function removePeriod(Period $period): self
    {
        if ($this->periods->contains($period)) {
            $this->periods->removeElement($period);
            // set the owning side to null (unless already changed)
            if ($period->getIdEra() === $this) {
                $period->setIdEra(null);
            }
        }

        return $this;
    }

    public function getUniverse(): ?Universe
    {
        return $this->universe;
    }

    public function setUniverse(?Universe $universe): self
    {
        $this->universe = $universe;

        return $this;
    }

    public function getLogoLinkA(): ?string
    {
        return $this->logoLinkA;
    }

    public function setLogoLinkA(string $logoLinkA): self
    {
        $this->logoLinkA = $logoLinkA;

        return $this;
    }

    public function getLogoLinkB(): ?string
    {
        return $this->logoLinkB;
    }

    public function setLogoLinkB(string $logoLinkB): self
    {
        $this->logoLinkB = $logoLinkB;

        return $this;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'universe' => $this->universe,
            'position' => $this->position,
            'name' => $this->name,
            'cleanName' => $this->cleanName,
            'image' => $this->image,
            'periods' => $this->periods,
            'logoLinkA' => $this->logoLinkA,
            'logoLinkB' => $this->logoLinkB,
        ];
    }
}
