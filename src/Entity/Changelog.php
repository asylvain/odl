<?php

namespace ODL\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ODL\Repository\ChangelogRepository")
 */
class Changelog
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", columnDefinition="timestamp DEFAULT current_timestamp() NOT NULL")
     */
    private $date;

    /**
     * @ORM\Column(type="text")
     */
    private $author;

    /**
     * @ORM\Column(type="text")
     */
    private $cl_type;

    /**
     * @ORM\Column(type="text")
     */
    private $name_universe;

    /**
     * @ORM\Column(type="text")
     */
    private $name_era;

    /**
     * @ORM\Column(type="text")
     */
    private $name_period;

    /**
     * @ORM\Column(type="text")
     */
    private $old_position;

    /**
     * @ORM\Column(type="text")
     */
    private $new_position;

    /**
     * @ORM\Column(type="text")
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $new_title;

    /**
     * @ORM\Column(type="text")
     */
    private $cover;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="text")
     */
    private $link_a;

    /**
     * @ORM\Column(type="text")
     */
    private $link_b;

    /**
     * @ORM\Column(type="text")
     */
    private $is_event;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(string $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getClType(): ?string
    {
        return $this->cl_type;
    }

    public function setClType(string $cl_type): self
    {
        $this->cl_type = $cl_type;

        return $this;
    }

    public function getNameUniverse(): ?string
    {
        return $this->name_universe;
    }

    public function setNameUniverse(string $name_universe): self
    {
        $this->name_universe = $name_universe;

        return $this;
    }

    public function getNameEra(): ?string
    {
        return $this->name_era;
    }

    public function setNameEra(string $name_era): self
    {
        $this->name_era = $name_era;

        return $this;
    }

    public function getNamePeriod(): ?string
    {
        return $this->name_period;
    }

    public function setNamePeriod(string $name_period): self
    {
        $this->name_period = $name_period;

        return $this;
    }

    public function getOldPosition(): ?string
    {
        return $this->old_position;
    }

    public function setOldPosition(string $old_position): self
    {
        $this->old_position = $old_position;

        return $this;
    }

    public function getNewPosition(): ?string
    {
        return $this->new_position;
    }

    public function setNewPosition(string $new_position): self
    {
        $this->new_position = $new_position;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getNewTitle(): ?string
    {
        return $this->new_title;
    }

    public function setNewTitle(string $new_title): self
    {
        $this->new_title = $new_title;

        return $this;
    }

    public function getCover(): ?string
    {
        return $this->cover;
    }

    public function setCover(string $cover): self
    {
        $this->cover = $cover;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getLinkA(): ?string
    {
        return $this->link_a;
    }

    public function setLinkA(string $link_a): self
    {
        $this->link_a = $link_a;

        return $this;
    }

    public function getLinkB(): ?string
    {
        return $this->link_b;
    }

    public function setLinkB(string $link_b): self
    {
        $this->link_b = $link_b;

        return $this;
    }

    public function getIsEvent(): ?string
    {
        return $this->is_event;
    }

    public function setIsEvent(string $is_event): self
    {
        $this->is_event = $is_event;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }
}
