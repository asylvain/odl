<?php

namespace ODL\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class Universes
{
    protected $universes;

    public function __construct()
    {
        $this->universes = new ArrayCollection();
    }

    public function getUniverses()
    {
        return $this->universes;
    }

    public function removeUniverse(Universe $universe)
    {
        $this->universes->removeElement($universe);
    }
}
