<?php

namespace ODL\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="universe")
 * @ORM\Entity(repositoryClass="ODL\Repository\UniverseRepository")
 */
class Universe
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=13)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="ODL\Core\RandomIdGenerator")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $cleanName;

    /**
     * @ORM\OneToMany(targetEntity="ODL\Entity\Era", mappedBy="universe", cascade={"remove"})
     */
    private $eras;

    public function __construct()
    {
        $this->eras = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCleanName(): ?string
    {
        return $this->cleanName;
    }

    public function setCleanName(string $cleanName): self
    {
        $this->cleanName = $cleanName;

        return $this;
    }

    /**
     * @return Collection|Era[]
     */
    public function getEras(): Collection
    {
        return $this->eras;
    }

    public function addEra(Era $era): self
    {
        if (!$this->eras->contains($era)) {
            $this->eras[] = $era;
            $era->setUniverse($this);
        }

        return $this;
    }

    public function removeEra(Era $era): self
    {
        if ($this->eras->contains($era)) {
            $this->eras->removeElement($era);
            // set the owning side to null (unless already changed)
            if ($era->getUniverse() === $this) {
                $era->setUniverse(null);
            }
        }

        return $this;
    }
}
