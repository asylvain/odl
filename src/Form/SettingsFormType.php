<?php

namespace ODL\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;

class SettingsFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('website_banner', FileType::class, [
                'label' => 'Bannière',
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '2000k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/jpg',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid image',
                    ]),
                ],
                'help' => 'jpeg/jpg, 2Mb max, width: ~2500px, height: 200px.',
            ])
            ->add('admin_sender_email', EmailType::class, [
                'required' => true,
                'label' => 'Adresse e-mail expéditeur',
                'help' => 'Adresse utilisée pour l\'envoi d\'e-mail venant du site. Peut être celle du site ou d\'un administrateur.',
            ])
            ->add('admin_sender_name', null, [
                'label' => 'Nom associé à l\'e-mail',
                'help' => 'Le nom affiché avec l\'e-mail. Peut être le nom du site ou celui d\'un administrateur. Peut rester vide.',
            ])
            ->add('save', SubmitType::class, [
                'attr' => ['class' => 'ui green button'],
            ]);
    }
}
