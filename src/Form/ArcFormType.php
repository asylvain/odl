<?php

namespace ODL\Form;

use ODL\Entity\Arc;
use ODL\Entity\Period;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class ArcFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('position', null, [
                'required' => false,
                'attr' => [
                    'min' => 1,
                    'data-content' => 'Laisser ce champ vide pour ajouter l\'arc en fin d\'ordre.',
                ],
            ])
            ->add('title', null, [
                'label' => 'Titre',
            ])
            ->add('cover', FileType::class, [
                'label' => 'Cover',
                'mapped' => false,
                'required' => true,
                'constraints' => [
                    new File([
                        'maxSize' => '1000k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/jpg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid image',
                    ]),
                ],
                'help' => 'jpeg/jpg/png, 1Mo max.',
            ])
            ->add('content', null, [
                'label' => 'Description',
            ])
            ->add('linkA', UrlType::class, [
                'label' => 'Premier lien',
                'required' => false,
                'empty_data' => '',
            ])
            ->add('linkB', UrlType::class, [
                'label' => 'Second lien',
                'required' => false,
                'empty_data' => '',
            ])
            ->add('isEvent', CheckboxType::class, [
                'label' => 'S\'agit-il d\'un event ? ',
                'required' => false,
            ])
            ->add('period', EntityType::class, [
                'label' => 'Période',
                'class' => Period::class,
                'choice_label' => 'name',
                'group_by' => function ($period) {
                    return "{$period->getEra()->getUniverse()->getName()} / {$period->getEra()->getName()}";
                },
                'placeholder' => 'Choisir une période',
            ])
            ->add('save', SubmitType::class, [
                'attr' => ['class' => 'ui green button'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Arc::class,
        ]);
    }
}
