<?php

namespace ODL\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class MaintenanceFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('active', CheckboxType::class, [
                'label' => 'Maintenance',
                'required' => false,
                'help' => 'Mettre le site en maintenance.',
            ])
            ->add('ttl', NumberType::class, [
                'label' => 'Temps de la maintenance',
                'required' => false,
                'html5' => true,
                'input' => 'string',
                'scale' => 0,
                'attr' => ['min' => 0],
                'help' => 'Optionnel : Définir le temps (en seconde) avant que le mode maintenance soit désactivé automatiquement.',
            ])
            ->add('save', SubmitType::class, [
                'attr' => ['class' => 'ui green button'],
            ]);
    }
}
