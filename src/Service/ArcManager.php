<?php

namespace ODL\Service;

use Doctrine\ORM\EntityManagerInterface;
use ODL\Entity\Arc;
use ODL\Entity\Era;
use ODL\Entity\Period;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ArcManager
{
    private $em;
    private $repo;
    private $fileManager;

    public function __construct(
        EntityManagerInterface $em,
        FileManager $fileManager
    ) {
        $this->em = $em;
        $this->repo = $this->em->getRepository(Arc::class);
        $this->fileManager = $fileManager;
    }

    public function findBy(array $criteria)
    {
        $arc = $this->repo->findBy($criteria);

        return $arc;
    }

    public function find($id)
    {
        $arc = $this->repo->find($id);

        return $arc;
    }

    public function sort($arcs)
    {
        $arcs_sorted = [];
        $i = 1;
        $right_id = false;
        $n = count($arcs);
        foreach ($arcs as $arc) {
            if (null === $arc->getLeftId()) {
                $first_arc = $arc;
                $right_id = $arc->getRightId();
                continue;
            }
            if (null === $arc->getRightId()) {
                $arcs_sorted[$n - 1] = $arc;
                continue;
            }
            if ($arc->getId() == $right_id) {
                $arcs_sorted[$i] = $arc;
                $right_id = $arc->getRightId();
                ++$i;
            }
        }
        $arcs_sorted[0] = $first_arc;
        ksort($arcs_sorted);

        return $arcs_sorted;
    }

    public function findLastArc(Period $period): ?Arc
    {
        $arc = null;
        $result = $this->repo->findLastArc($period->getId());

        if (!empty($result)) {
            $arc = $result[0];
        }

        return $arc;
    }

    public function findArcsFromPeriodBetweenPositions(string $periodId, $first, $last): array
    {
        $arcs = $this->repo->findArcsFromPeriodBetweenPositions($periodId, $first, $last);

        return $arcs;
    }

    public function createArc(Arc $arc, UploadedFile $file): array
    {
        $updated = false;
        $error = false;
        $type = '';
        $message = '';

        // Set position
        $lastArc = $this->findLastArc($arc->getPeriod());

        if ($lastArc) {
            if (($arc->getPosition() > ($lastArc->getPosition() + 1)) || (!$arc->getPosition())) {
                $arc->setPosition($lastArc->getPosition() + 1);
            } elseif ($arc->getPosition() <= $lastArc->getPosition()) {
                $this->replaceArcsForCreate($arc, 'plus');
            } else {
                throw new \Exception('Bad value for position.', 1);
            }
        } else {
            $arc->setPosition(1);
        }

        // Upload cover
        $this->fileManager->setTargetDirectory('arc.covers');
        $upload = $this->fileManager->upload($file);

        if (!$upload['error']) {
            $arc->setCover($upload['filename']);
            $this->em->persist($arc);
            $this->em->flush();
            $updated = true;
            $type = 'success';
            $message = "L'arc {$arc->getTitle()} a bien été créé.";
        } else {
            $error = $upload['error'];
            $type = 'error';
            $message = $upload['message'];
        }

        return [
            'updated' => $updated,
            'error' => $error,
            'type' => $type,
            'message' => $message,
        ];
    }

    public function replaceArcsForCreate(Arc $arc, string $type)
    {
        $arcsFromCurrentPeriod = $arc->getPeriod()->getArcs();

        foreach ($arcsFromCurrentPeriod as $arcFromCurrentPeriod) {
            $currentPosition = $arcFromCurrentPeriod->getPosition();

            if ($currentPosition >= $arc->getPosition()) {
                if ('plus' == $type) {
                    $arcFromCurrentPeriod->setPosition(++$currentPosition);
                } elseif ('minus' == $type) {
                    $arcFromCurrentPeriod->setPosition(--$currentPosition);
                }
                $this->em->persist($arcFromCurrentPeriod);
            }
        }

        $this->em->flush();
    }

    public function replaceArcsForUpdate(Arc $arc, int $positionToCompare = null)
    {
        $periodId = $arc->getPeriod()->getId();
        $position = $arc->getPosition();

        if ($position < --$positionToCompare) {
            $arcsFromCurrentPeriod = $this->findArcsFromPeriodBetweenPositions($periodId, $position, $positionToCompare);
            foreach ($arcsFromCurrentPeriod as $arcFromCurrentPeriod) {
                $currentPosition = $arcFromCurrentPeriod->getPosition();
                $arcFromCurrentPeriod->setPosition(++$currentPosition);
                $this->em->persist($arcFromCurrentPeriod);
            }
        } elseif ($position > ++$positionToCompare) {
            $arcsFromCurrentPeriod = $this->findArcsFromPeriodBetweenPositions($periodId, ++$positionToCompare, $position);
            foreach ($arcsFromCurrentPeriod as $arcFromCurrentPeriod) {
                $currentPosition = $arcFromCurrentPeriod->getPosition();
                $arcFromCurrentPeriod->setPosition(--$currentPosition);
                $this->em->persist($arcFromCurrentPeriod);
            }
        }

        $this->em->flush();
    }

    public function getCovers(array $ids): array
    {
        return $this->repo->getCovers($ids);
    }

    public function remove(Arc $arc)
    {
        $position = $arc->getPosition();
        $cover = $arc->getCover();
        $lastArc = $this->findLastArc($arc->getPeriod());

        if ($position < $lastArc->getPosition()) {
            $this->replaceArcsForCreate($arc, 'minus');
        }

        $this->em->remove($arc);
        $this->em->flush();

        $this->fileManager->setTargetDirectory('arc.covers');
        $this->fileManager->remove($cover);
    }

    public function update(Arc $arcSubmitted, ?UploadedFile $filesSubmitted, int $oldPosition): array
    {
        $updated = false;
        $type = 'info';
        $message = 'Aucune modification.';
        $upload = [];

        if ($filesSubmitted) {
            $actualCover = $arcSubmitted->getCover();

            $this->fileManager->setTargetDirectory('arc.covers');
            $this->fileManager->remove($actualCover);
            $upload = $this->fileManager->upload($filesSubmitted);

            if ($upload['error']) {
                return [
                    'updated' => $updated,
                    'error' => $upload['error'],
                    'type' => 'error',
                    'message' => $upload['message'],
                ];
            } else {
                $arcSubmitted->setCover($upload['filename']);
            }
        }

        $this->replaceArcsForUpdate($arcSubmitted, $oldPosition);

        $this->em->persist($arcSubmitted);
        $this->em->flush();
        $updated = true;
        $type = 'success';
        $message = 'Vos modifications ont bien été prises en compte.';

        return [
            'updated' => $updated,
            'error' => false,
            'type' => $type,
            'message' => $message,
        ];
    }

    public function findArcsByQuery(string $query, Era $era = null): array
    {
        $periodIds = [];

        if (isset($era)) {
            $periods = $era->getPeriods();
            foreach ($periods as $period) {
                $periodIds[] = $period->getId();
            }
        }

        return $this->repo->findArcsByQuery($query, $periodIds);
    }
}
