<?php

namespace ODL\Service;

use Doctrine\ORM\EntityManagerInterface;
use ODL\Entity\Setting;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class SettingManager
{
    private $em;
    private $repo;
    private $fileManager;

    public function __construct(
        EntityManagerInterface $em,
        FileManager $fileManager
    ) {
        $this->em = $em;
        $this->repo = $this->em->getRepository(Setting::class);
        $this->fileManager = $fileManager;
    }

    public function findBy(array $criteria): array
    {
        $setting = $this->repo->findBy($criteria);

        return $setting;
    }

    public function findOneBy(array $criteria): ?Setting
    {
        $setting = $this->repo->findOneBy($criteria);

        return $setting;
    }

    public function findAll(): array
    {
        $settings = [];
        $results = $this->repo->findAll();

        foreach ($results as $result) {
            $settings[$result->getParam()] = $result->getValue();
        }

        return $settings;
    }

    public function update(array $dataSubmitted)
    {
        $updated = false;
        $error = false;
        $type = '';
        $message = '';

        foreach ($dataSubmitted as $param => $value) {
            if ($value instanceof UploadedFile) {
                $this->fileManager->setTargetDirectory("setting.{$param}");
                $upload = $this->fileManager->upload($value, $param);

                if ($upload['error']) {
                    $error = $upload['error'];
                    $type = 'error';
                    $message = $upload['message'];

                    continue;
                }

                $result = $this->findBy(['param' => $param]);
                if (empty($result)) {
                    $setting = (new Setting())
                        ->setParam($param)
                        ->setValue($upload['filename']);
                    $this->em->persist($setting);
                } elseif (count($result) < 1) {
                    // todo: handle error
                }

                $updated = true;
            } elseif (null !== $value) {
                $result = $this->findBy(['param' => $param]);
                if (empty($result)) {
                    $setting = (new Setting())
                        ->setParam($param)
                        ->setValue((string) $value);
                    $this->em->persist($setting);
                } elseif (1 == count($result)) {
                    $setting = $result[0]->setValue((string) $value);
                    $this->em->persist($setting);
                    $updated = true;
                } else {
                    // todo: handle error
                }
            }
        }

        $this->em->flush();

        if ($updated && empty($message)) {
            $type = 'info';
            $message = 'Vos modifications ont été prises en comptes.';
        }

        return [
            'updated' => $updated,
            'error' => $error,
            'type' => $type,
            'message' => $message,
        ];
    }
}
