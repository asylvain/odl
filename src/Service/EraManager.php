<?php

namespace ODL\Service;

use Doctrine\ORM\EntityManagerInterface;
use ODL\Core\StringModifier;
use ODL\Entity\Era;
use ODL\Entity\Eras;
use ODL\Entity\Universe;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class EraManager
{
    private $em;
    private $stringModifier;
    private $fileManager;
    private $repo;

    public function __construct(
        EntityManagerInterface $em,
        StringModifier $stringModifier,
        FileManager $fileManager
    ) {
        $this->em = $em;
        $this->stringModifier = $stringModifier;
        $this->fileManager = $fileManager;
        $this->repo = $this->em->getRepository(Era::class);
    }

    public function upsertEra(Eras $eras, array $filesSubmitted, Universe $parent): array
    {
        $updated = false;
        $type = 'info';
        $message = 'Aucune modification.';
        $upload = [];

        foreach ($eras->getEras() as $id => $eraUpdated) {
            $cleanName = $this->stringModifier->cleanNameGenerator($eraUpdated->getName());

            if (!$eraUpdated->getCleanName()) {
                $eraUpdated->setPosition(0);
                $eraUpdated->setCleanName($cleanName);
                $eraUpdated->setUniverse($parent);

                $upload = $this->updateImages($eraUpdated, $filesSubmitted[$id]);

                if ($upload['error']) {
                    return [
                        'updated' => $updated,
                        'error' => $upload['error'],
                        'type' => 'error',
                        'message' => $upload['message'],
                    ];
                }

                $this->em->persist($eraUpdated);
                $updated = true;
            } else {
                if ($eraUpdated->getName() != $cleanName) {
                    $eraUpdated->setCleanName($cleanName);
                    $updated = true;
                }

                $upload = $this->updateImages($eraUpdated, $filesSubmitted[$id]);

                if ($upload['error']) {
                    return [
                        'updated' => $updated,
                        'error' => $upload['error'],
                        'type' => 'error',
                        'message' => $upload['message'],
                    ];
                }

                $updated = true;

                if ($updated) {
                    $this->em->persist($eraUpdated);
                    $updated = true;
                }
            }
        }

        if ($updated) {
            $this->em->flush();
            $type = 'success';
            $message = 'Vos modifications ont bien été prises en compte.';
        }

        return [
            'updated' => $updated,
            'error' => false,
            'type' => $type,
            'message' => $message,
        ];
    }

    public function updateImages(Era $era, array $filesSubmitted): array
    {
        $upload = ['error' => false];

        if (!empty($filesSubmitted['image'])) {
            $upload = $this->updateBanner($era, $filesSubmitted['image']);

            if ($upload['error']) {
                return $upload;
            }
        }

        if (!empty($filesSubmitted['logoLinkA']) || !empty($filesSubmitted['logoLinkB'])) {
            $upload = $this->updateLogos(
                $era,
                [
                    'logoLinkA' => $filesSubmitted['logoLinkA'],
                    'logoLinkB' => $filesSubmitted['logoLinkB'],
                ]
            );

            if ($upload['error']) {
                return $upload;
            }
        }

        return $upload;
    }

    public function updateBanner(Era $era, UploadedFile $fileSubmitted): array
    {
        $this->fileManager->setTargetDirectory('era.banner');
        $actualBanner = $era->getImage();

        $upload = $this->fileManager->upload($fileSubmitted);

        if (!$upload['error']) {
            if ($actualBanner) {
                $this->fileManager->remove($actualBanner);
            }
            $filename = $upload['filename'];
            $era->setImage($filename);
        }

        return $upload;
    }

    public function updateLogos(Era $era, array $filesSubmitted): array
    {
        $this->fileManager->setTargetDirectory('era.logo');

        if ($filesSubmitted['logoLinkA']) {
            $upload = $this->fileManager->upload($filesSubmitted['logoLinkA']);

            if (!$upload['error']) {
                $actualLogoLinkA = $era->getLogoLinkA();
                if ($actualLogoLinkA) {
                    $this->fileManager->remove($actualLogoLinkA);
                }
                $filename = $upload['filename'];
                $era->setLogoLinkA($filename);
            } else {
                return $upload;
            }
        }

        if ($filesSubmitted['logoLinkB']) {
            $upload = $this->fileManager->upload($filesSubmitted['logoLinkB']);

            if (!$upload['error']) {
                $actualLogoLinkB = $era->getLogoLinkB();
                if ($actualLogoLinkB) {
                    $this->fileManager->remove($actualLogoLinkB);
                }
                $filename = $upload['filename'];
                $era->setLogoLinkB($filename);
            } else {
                return $upload;
            }
        }

        return $upload;
    }

    public function findAll()
    {
        $eras = $this->repo->findAll();

        return $eras;
    }

    public function remove($id)
    {
        $era = $this->repo->find($id);
        $banner = $era->getImage();
        $logoA = $era->getLogoLinkA();
        $logoB = $era->getLogoLinkB();

        $this->em->remove($era);
        $this->em->flush();

        $this->fileManager->setTargetDirectory('era.banner');
        $this->fileManager->remove($banner);
        $this->fileManager->setTargetDirectory('era.logo');
        $this->fileManager->remove($logoA);
        $this->fileManager->remove($logoB);
    }

    public function findOne($id)
    {
        $era = $this->repo->find($id);

        return $era;
    }

    public function find($id)
    {
        $era = $this->repo->find($id);

        return $era;
    }

    public function findBy(array $criteria)
    {
        $eras = new Eras();
        $allEras = $this->repo->findBy($criteria);
        foreach ($allEras as $era) {
            $eras->getEras()->add($era);
        }

        return $eras;
    }

    public function updateName($dataSubmitted): bool
    {
        $updated = false;

        foreach ($dataSubmitted->getEras() as $eraUpdated) {
            $cleanName = $this->stringModifier->cleanNameGenerator($eraUpdated->getName());

            if ($eraUpdated->getCleanName() != $cleanName) {
                $eraUpdated->setCleanName($cleanName);
                $this->em->persist($eraUpdated);

                $updated = true;
            }
        }

        return $updated;
    }
}
