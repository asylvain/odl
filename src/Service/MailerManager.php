<?php

namespace ODL\Service;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\NamedAddress;

class MailerManager
{
    private $mailer;
    private $email;
    private $sender;
    private $settingManager;
    private $banner;

    public function __construct(MailerInterface $mailer, SettingManager $settingManager)
    {
        $this->mailer = $mailer;
        $this->settingManager = $settingManager;
        $this->setSender();
        $this->setEmail();
    }

    private function setSender(): self
    {
        $address = $this->settingManager->findOneBy(['param' => 'admin_sender_email'])->getValue();
        $name = $this->settingManager->findOneBy(['param' => 'admin_sender_name']);

        $this->sender = $name ? new NamedAddress($address, $name->getValue()) : new Address($address);

        return $this;
    }

    private function setEmail(): self
    {
        $this->email = (new TemplatedEmail())
            ->from($this->sender);

        return $this;
    }

    public function getEmail(): TemplatedEmail
    {
        return $this->email;
    }

    private function setBanner(): self
    {
        $banner = $this->settingManager->findOneBy(['param' => 'website_banner']);

        if ($banner) {
            $this->banner = $banner->getValue();
        }

        return $this;
    }

    private function setForgottenPasswordEmail(string $recipient, array $args): self
    {
        $this->setBanner();

        if ($this->banner) {
            $args = array_merge($args, ['banner' => $this->banner]);
        }

        $this->email->to($recipient)
            ->subject('Réinitialisez votre mot de passe')
            ->htmlTemplate('emails/forgotten-password.html.twig')
            ->context($args);

        return $this;
    }

    public function sendForgottenPasswordEmail(string $recipient, array $args)
    {
        $this->setForgottenPasswordEmail($recipient, $args);

        $this->mailer->send($this->email);

        // Reset email
        $this->setEmail();
    }
}
