<?php

namespace ODL\Service;

use ODL\Core\StringModifier;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileManager
{
    private $params;
    private $stringModifier;
    private $targetDirectory;
    private $imageManager;
    private $type;

    public function __construct(
        ContainerBagInterface $params,
        StringModifier $stringModifier,
        ImageManager $imageParameter
    ) {
        $this->params = $params;
        $this->stringModifier = $stringModifier;
        $this->imageManager = $imageParameter;
    }

    public function setTargetDirectory(string $type): self
    {
        $this->type = $type;

        if ('arc.covers' === $this->type) {
            $this->targetDirectory = $this->params->get('app.assets_image_covers');
        } elseif ('era.banner' === $this->type) {
            $this->targetDirectory = $this->params->get('app.assets_image_sections');
        } elseif ('era.logo' === $this->type) {
            $this->targetDirectory = $this->params->get('app.assets_image_logos');
        } elseif ('setting.website_banner' === $this->type) {
            $this->targetDirectory = $this->params->get('app.assets_image');
        } elseif ('user.avatar' === $this->type) {
            $this->targetDirectory = $this->params->get('app.assets_image_user_avatars');
        }

        return $this;
    }

    public function upload(UploadedFile $file, string $filename = ''): array
    {
        if (empty($filename)) {
            $filename = "{$this->stringModifier->image_name_generator()}.{$file->guessExtension()}";
        } else {
            $filename = "$filename.{$file->guessExtension()}";
        }

        try {
            $this->imageManager->setParameters($this->type);
            $this->imageManager->resize($file);
            $file->move($this->targetDirectory, $filename);
        } catch (FileException $e) {
            return ['error' => true, 'message' => $e->getMessage()];
        }

        return ['error' => false, 'filename' => $filename];
    }

    public function upload64(string $data64, string $filename = ''): array
    {
        $data64 = explode(';base64,', $data64);
        $base64 = $data64[1];
        $extension = str_replace('data:image/', '', $data64[0]);
        if (empty($filename)) {
            $filename = "{$this->targetDirectory}/{$this->stringModifier->image_name_generator()}.$extension";
        } else {
            $filename = "{$this->targetDirectory}/$filename.$extension";
        }

        $data = base64_decode($base64);
        $imageSrc = imagecreatefromstring($data);

        try {
            if ('png' == $extension) {
                $width = imagesx($imageSrc);
                $height = imagesy($imageSrc);
                $image = imagecreatetruecolor($width, $height);
                imagesavealpha($image, true);
                $color = imagecolorallocatealpha($image, 0, 0, 0, 127);
                imagefill($image, 0, 0, $color);
                imagecopy($image, $imageSrc, 0, 0, 0, 0, $width, $height);
                imagepng($image, $filename, 7);
                imagedestroy($image);
            } elseif (in_array($extension, ['jpeg', 'jpg'])) {
                imagejpeg($imageSrc, $filename, 70);
                imagedestroy($imageSrc);
            }
        } catch (FileException $e) {
            return ['error' => true, 'message' => $e->getMessage()];
        }

        return ['error' => false, 'filename' => basename($filename)];
    }

    public function remove(string $filename)
    {
        $targetFile = $this->targetDirectory.'/'.$filename;
        if (!empty($filename) && file_exists($targetFile)) {
            unlink($targetFile);
        }
    }
}
