<?php

namespace ODL\Service;

use Doctrine\ORM\EntityManagerInterface;
use ODL\Entity\Changelog;

class ChangelogManager
{
    private $em;
    private $repo;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repo = $this->em->getRepository(Changelog::class);
    }

    public function findAll()
    {
        $changelog = $this->repo->findAll();

        return $changelog;
    }
}
