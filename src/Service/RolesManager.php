<?php

namespace ODL\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;

class RolesManager
{
    private $hierarchy;

    public function __construct(
        ContainerInterface $params
    ) {
        $this->hierarchy = $params->getParameter('security.role_hierarchy.roles');
    }

    public function getHierarchy()
    {
        return $this->hierarchy;
    }

    public function findAll(): array
    {
        $roles = [];

        foreach ($this->hierarchy as $role => $value) {
            $roles[] = $role;

            foreach ($value as $role) {
                $roles[] = $role;
            }
        }

        return array_unique($roles);
    }

    public function findAllForChoices(): array
    {
        $roles = [];

        foreach ($this->findAll() as $role) {
            $roles[$role] = $role;
        }

        return $roles;
    }

    public function sortRolesByHierarchy(array $roles): array
    {
        $childRoles = [];
        $parentRoles = [];
        $rolesToReturn = [];

        foreach ($roles as $role) {
            if (!isset($this->hierarchy[$role])) {
                $childRoles[] = $role;
            } else {
                $parentRoles[] = $role;
            }
        }

        foreach ($childRoles as $childRole) {
            if (!empty($parentRoles)) {
                foreach ($parentRoles as $parentRole) {
                    $rolesToReturn[] = $parentRole;
                    if (!in_array($childRole, $this->hierarchy[$parentRole])) {
                        $rolesToReturn[] = $childRole;
                    }
                }
            } else {
                $rolesToReturn[] = $childRole;
            }
        }

        return array_unique($rolesToReturn);
    }
}
