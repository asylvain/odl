<?php

namespace ODL\Service;

use Lexik\Bundle\MaintenanceBundle\Drivers\DriverFactory;

class MaintenanceManager
{
    private $driver;

    public function __construct(DriverFactory $driverFactory)
    {
        $this->driver = $driverFactory->getDriver();
    }

    public function getDriver()
    {
        return $this->driver;
    }

    public function isExists(): ?bool
    {
        return $this->driver->isExists();
    }

    public function getTtl(): ?bool
    {
        return $this->driver->getTtl();
    }

    public function handleMaintenance(bool $active, ?string $ttl): string
    {
        $message = '';
        if ($active) {
            if ((null !== $this->driver->isExists()) && $this->driver->isExists()) {
                $message = 'Le site est déjà en maintenance.';
            } else {
                $messageTtl = 'pour une durée indéterminée.';
                if (isset($ttl)) {
                    $this->driver->setTtl($ttl);
                    $messageTtl = "pour {$ttl}s.";
                }
                $this->driver->getMessageLock($this->driver->lock());
                $message = "Le site a bien été mis sous maintenance {$messageTtl}";
            }
        } else {
            $this->driver->unlock();
            $message = 'Le site n\'est plus sous maintenance.';
        }

        return $message;
    }
}
