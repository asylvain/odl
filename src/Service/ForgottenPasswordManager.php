<?php

namespace ODL\Service;

use Doctrine\ORM\EntityManagerInterface;
use ODL\Entity\User;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class ForgottenPasswordManager
{
    private $em;
    private $userRepository;
    private $tokenGenerator;

    public function __construct(
        EntityManagerInterface $em,
        TokenGeneratorInterface $tokenGenerator
    ) {
        $this->em = $em;
        $this->userRepository = $this->em->getRepository(User::class);
        $this->tokenGenerator = $tokenGenerator;
    }

    public function getForgottenPasswordToken(User $user): string
    {
        $now = new \DateTime('now');
        $token = $this->tokenGenerator->generateToken().'date'.$now->format('YmdHis');

        $user->setForgottenPassword($token);
        $this->em->persist($user);
        $this->em->flush();

        return $token;
    }

    public function isValidDate(string $dateString): bool
    {
        $date = new \DateTime($dateString);
        $now = new \DateTime('now');
        $tenMinutesAgo = $now->sub(new \DateInterval('PT10M'));

        return $date > $tenMinutesAgo;
    }
}
