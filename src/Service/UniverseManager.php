<?php

namespace ODL\Service;

use Doctrine\ORM\EntityManagerInterface;
use ODL\Core\StringModifier;
use ODL\Entity\Universe;
use ODL\Entity\Universes;

class UniverseManager
{
    private $em;
    private $stringModifier;
    private $repo;

    public function __construct(
        EntityManagerInterface $em,
        StringModifier $stringModifier
    ) {
        $this->em = $em;
        $this->stringModifier = $stringModifier;
        $this->repo = $this->em->getRepository(Universe::class);
    }

    public function find($id)
    {
        $universe = $this->repo->find($id);

        return $universe;
    }

    public function upsertUniverse(Universes $universes): bool
    {
        $update = false;

        foreach ($universes->getUniverses() as $universeUpdated) {
            $cleanName = $this->stringModifier->cleanNameGenerator($universeUpdated->getName());

            if (!$universeUpdated->getCleanName()) {
                $universeUpdated->setCleanName($cleanName);

                $this->em->persist($universeUpdated);
                $update = true;
            } elseif ($universeUpdated->getCleanName() != $cleanName) {
                $universeUpdated->setCleanName($cleanName);

                $this->em->persist($universeUpdated);
                $update = true;
            }
        }

        return $update;
    }

    public function findAll()
    {
        $universes = new Universes();
        $allUniverses = $this->repo->findAll();
        foreach ($allUniverses as $universe) {
            $universes->getUniverses()->add($universe);
        }

        return $universes;
    }

    public function findOne(string $id)
    {
        $universe = $this->repo->find($id);

        return $universe;
    }

    public function findBy(array $criteria): array
    {
        $universe = $this->repo->findBy($criteria);

        return $universe;
    }

    public function remove($id)
    {
        $era = $this->repo->find($id);
        $this->em->remove($era);

        return $this->em->flush();
    }
}
