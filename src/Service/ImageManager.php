<?php

namespace ODL\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageManager
{
    private $parameters;
    private $size;
    private $type;

    public function __construct(array $parameters)
    {
        $this->parameters = $parameters;
    }

    public function setParameters(string $type): self
    {
        if (isset($this->parameters[$type])) {
            $this->size = $this->parameters[$type]['size'];
            $this->type = $this->parameters[$type]['type'];
        }

        return $this;
    }

    /**
     * Resize cover
     * http://memo-web.fr/categorie-php-197.php.
     */
    public function resize(UploadedFile $file): bool
    {
        $source = $file->getPathname();

        // Fetch image size
        if (!(list($source_largeur, $source_hauteur) = @getimagesize($source))) {
            return false;
        }

        // Calculation of the dynamic value according to the current dimensions of the image
        // and the fixed dimension that we specified as an parameter.
        if ('height' == $this->type) {
            $nouv_hauteur = $this->size;
            $nouv_largeur = ($this->size / $source_hauteur) * $source_largeur;
        } else {
            $nouv_largeur = $this->size;
            $nouv_hauteur = ($this->size / $source_largeur) * $source_hauteur;
        }

        // Container creation
        $image = \imagecreatetruecolor($nouv_largeur, $nouv_hauteur);
        \imagealphablending($image, false);
        \imagesavealpha($image, true);

        // Image source importation
        $source_image = \imagecreatefromstring(file_get_contents($source));

        // Copy the image to the new container by resampling it.
        \imagecopyresampled($image, $source_image, 0, 0, 0, 0, $nouv_largeur, $nouv_hauteur, $source_largeur, $source_hauteur);

        // Set free of the memory allocated to the source image.
        \imagedestroy($source_image);

        // Replace the uploaded image by the new image resized.
        if ('image/png' == $file->getMimeType()) {
            $resultat = \imagepng($image, $file->getPathname(), 7);
        } else {
            $resultat = \imagejpeg($image, $file->getPathname(), 70);
        }
        if (!$resultat) {
            return false;
        } else {
            // Set free of the memory allocated to the new image.
            \imagedestroy($image);

            return true;
        }
    }
}
