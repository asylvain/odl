<?php

namespace ODL\Service;

use Doctrine\ORM\EntityManagerInterface;
use ODL\Core\StringModifier;
use ODL\Entity\Era;
use ODL\Entity\Period;
use ODL\Entity\Periods;

class PeriodManager
{
    private $em;
    private $stringModifier;
    private $repo;

    public function __construct(
        EntityManagerInterface $em,
        StringModifier $stringModifier
    ) {
        $this->em = $em;
        $this->stringModifier = $stringModifier;
        $this->repo = $this->em->getRepository(Period::class);
    }

    public function findOne($id)
    {
        $period = $this->repo->find($id);

        return $period;
    }

    public function find($id)
    {
        $period = $this->repo->find($id);

        return $period;
    }

    public function findBy(array $criteria)
    {
        $periods = new Periods();
        $allPeriods = $this->repo->findBy($criteria);
        foreach ($allPeriods as $period) {
            $periods->getPeriods()->add($period);
        }

        return $periods;
    }

    public function remove($id)
    {
        $period = $this->repo->find($id);
        $this->em->remove($period);

        return $this->em->flush();
    }

    public function upsertPeriod(Periods $periods, Era $parent): array
    {
        $updated = false;
        $type = 'info';
        $message = 'Aucune modification.';

        foreach ($periods->getPeriods() as $periodUpdated) {
            $cleanName = $this->stringModifier->cleanNameGenerator($periodUpdated->getName());

            if (!$periodUpdated->getCleanName()) {
                $periodUpdated->setCleanName($cleanName);
                $periodUpdated->setPosition(0);
                $periodUpdated->setEra($parent);

                $this->em->persist($periodUpdated);
                $updated = true;
            } elseif ($periodUpdated->getCleanName() != $cleanName) {
                $periodUpdated->setCleanName($cleanName);

                $this->em->persist($periodUpdated);
                $updated = true;
            }
        }

        if ($updated) {
            $this->em->flush();
            $type = 'success';
            $message = 'Vos modifications ont bien été prises en compte.';
        }

        return [
            'updated' => $updated,
            'error' => false,
            'type' => $type,
            'message' => $message,
        ];
    }
}
