<?php

namespace ODL\Service;

use Doctrine\ORM\EntityManagerInterface;
use ODL\Entity\User;

class UserManager
{
    private $em;
    private $repo;

    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;
        $this->repo = $this->em->getRepository(User::class);
    }

    public function findBy(array $criteria): array
    {
        $user = $this->repo->findBy($criteria);

        return $user;
    }

    public function findOneBy(array $criteria): ?User
    {
        $user = $this->repo->findOneBy($criteria);

        return $user;
    }

    public function find($id): ?User
    {
        $user = $this->repo->find($id);

        return $user;
    }

    public function findAll(): array
    {
        return $this->repo->findAll();
    }

    public function update(User $user, string $avatar64 = '', FileManager $fileManager): array
    {
        $updated = false;
        $error = false;
        $type = 'info';
        $message = 'Vos modifications ont été prises en comptes.';

        if (!empty($avatar64)) {
            $fileManager->setTargetDirectory('user.avatar');
            $upload = $fileManager->upload64($avatar64);

            if (!isset($upload['filename'])) {
                $error = $upload['error'];
                $type = 'error';
                $message = $upload['message'];
            } else {
                if ($user->getAvatar()) {
                    $fileManager->remove($user->getAvatar());
                }
                $user->setAvatar($upload['filename']);
            }
        }

        $this->em->persist($user);
        $this->em->flush();

        return [
            'updated' => $updated,
            'error' => $error,
            'type' => $type,
            'message' => $message,
        ];
    }

    public function removeAvatar(User $user, FileManager $fileManager): bool
    {
        $fileManager->setTargetDirectory('user.avatar');
        $fileManager->remove($user->getAvatar());
        $user->setAvatar(null);

        $this->em->persist($user);
        $this->em->flush();

        return true;
    }
}
