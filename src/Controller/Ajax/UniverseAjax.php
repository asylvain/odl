<?php

namespace ODL\Controller\Ajax;

use ODL\Service\UniverseManager as UniverseManagerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * UniverseAjax.
 *
 * @Route("/ajax/universe")
 */
class UniverseAjax extends AbstractController
{
    private $universeManager;

    public function __construct(UniverseManagerService $universeManager)
    {
        $this->universeManager = $universeManager;
    }

    /**
     * delete_universe.
     *
     * @Route("/delete-universe", name="odl_ajax_universe_delete_universe",
     * methods={"DELETE"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     */
    public function deleteUniverse(Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();

        if ($isAjax) {
            $id = $request->get('id');
            $this->universeManager->remove($id);
        }

        return new JsonResponse();
    }
}
