<?php

namespace ODL\Controller\Ajax;

use ODL\Service\FileManager;
use ODL\Service\UserManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * UserAjax.
 *
 * @Route("/ajax/user")
 */
class UserAjax extends AbstractController
{
    private $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * removeAvatar.
     *
     * @Route("/remove-avatar", name="odl_ajax_user_avatar_remove",
     * methods={"DELETE"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     */
    public function removeAvatar(Request $request, FileManager $fileManager)
    {
        $isAjax = $request->isXMLHttpRequest();
        if ($isAjax) {
            $id = $request->get('id');
            $user = $this->userManager->find($id);
            $this->userManager->removeAvatar($user, $fileManager);
        }

        return new JsonResponse();
    }
}
