<?php

namespace ODL\Controller\Ajax;

use ODL\Form\ArcFormType;
use ODL\Service\ArcManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * ArcAjax.
 *
 * @Route("/ajax/arc")
 */
class ArcAjax extends AbstractController
{
    private $arcManager;

    public function __construct(ArcManager $arcManager)
    {
        $this->arcManager = $arcManager;
    }

    /**
     * get_covers.
     *
     * @Route("/get-covers", name="odl_ajax_arc_get_covers",
     * methods={"POST"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     */
    public function get_covers(Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();
        if ($isAjax) {
            $covers = [];
            $ids = $request->get('ids');
            $getCovers = $this->arcManager->getCovers($ids);

            $covers = array_map(function ($cover) {
                return 'assets/img/covers/'.$cover['cover'];
            }, $getCovers);
        }

        return new JsonResponse($covers);
    }

    /**
     * remove.
     *
     * @Route("/remove", name="odl_ajax_arc_remove",
     * methods={"DELETE"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     * @IsGranted("ROLE_EDITOR")
     */
    public function remove(Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();
        if ($isAjax) {
            $id = $request->get('id');
            $arc = $this->arcManager->find($id);
            $this->arcManager->remove($arc);
        }

        return new JsonResponse();
    }

    /**
     * update.
     *
     * @Route("/update/{id}", name="odl_ajax_arc_update",
     * methods={"POST", "GET"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     * @IsGranted("ROLE_EDITOR")
     */
    public function update(int $id, Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();
        if ($isAjax) {
            $twig_args = [];
            $arc = $this->arcManager->find($id);
            $oldPosition = $arc->getPosition();
            $form = $this->createForm(ArcFormType::class, $arc);

            if ('POST' == $request->getMethod()) {
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $arcSubmitted = $form->getData();
                    $coverSubmitted = $request->files->get('arc_form')['cover'];
                    $response = $this->arcManager->update($arcSubmitted, $coverSubmitted, $oldPosition);
                    $arc = $this->arcManager->find($arcSubmitted->getId());
                    $response['arc'] = $arc;
                }
            } else {
                $twig_args = [
                    'form' => $form->createView(),
                    'arc' => $arc,
                ];

                $response = $this->renderView('admin/update_arc.html.twig', $twig_args);
            }
        }

        return new JsonResponse($response);
    }

    /**
     * getArcsByPeriod.
     *
     * @Route("/arc/{id}", name="odl_ajax_arc_get",
     * methods={"GET"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     */
    public function get(string $id)
    {
        $arc = $this->arcManager->find($id);

        return new JsonResponse($arc);
    }

    /**
     * getArcsByPeriod.
     *
     * @Route("/arcs-from-period", name="odl_ajax_arcsByPeriod",
     * methods={"GET"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     */
    public function getArcsByPeriod(Request $request)
    {
        $arcs = [];
        $isAjax = $request->isXMLHttpRequest();

        if ($isAjax) {
            $periodId = $request->get('id');
            $arcs = $this->arcManager->findBy(['period' => $periodId]);
        }

        return new JsonResponse($arcs);
    }
}
