<?php

namespace ODL\Controller\Ajax;

use ODL\Service\PeriodManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * PeriodAjax.
 *
 * @Route("/ajax/period")
 */
class PeriodAjax extends AbstractController
{
    private $periodManager;

    public function __construct(PeriodManager $periodManager)
    {
        $this->periodManager = $periodManager;
    }

    /**
     * delete_period.
     *
     * @Route("/delete-period", name="odl_ajax_period_delete_period",
     * methods={"DELETE"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     */
    public function delete_period(Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();

        if ($isAjax) {
            $id = $request->get('id');
            $this->periodManager->remove($id);
        }

        return new JsonResponse();
    }
}
