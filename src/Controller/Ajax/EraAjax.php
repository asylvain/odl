<?php

namespace ODL\Controller\Ajax;

use ODL\Service\EraManager;
use ODL\Service\UniverseManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * EraAjax.
 *
 * @Route("/ajax/era")
 */
class EraAjax extends AbstractController
{
    private $universeManager;
    private $eraManager;

    public function __construct(
        UniverseManager $universeManager,
        EraManager $eraManager
    ) {
        $this->universeManager = $universeManager;
        $this->eraManager = $eraManager;
    }

    /**
     * delete_era.
     *
     * @Route("/delete-era", name="odl_ajax_era_delete_era",
     * methods={"DELETE"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     */
    public function deleteEra(Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();

        if ($isAjax) {
            $id = $request->get('id');
            $this->eraManager->remove($id);
        }

        return new JsonResponse();
    }

    /**
     * get_era.
     *
     * @Route("/get-era", name="odl_ajax_era_get_era",
     * methods={"GET"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     */
    public function getEra(Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();
        $era = [];

        if ($isAjax) {
            $id = $request->get('id');
            $era = $this->eraManager->findOne($id);
        }

        return new JsonResponse($era);
    }
}
