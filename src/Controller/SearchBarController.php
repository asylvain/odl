<?php

namespace ODL\Controller;

use ODL\Service\ArcManager;
use ODL\Service\EraManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/search")
 */
class SearchBarController extends AbstractController
{
    private $arcManager;

    public function __construct(ArcManager $arcManager)
    {
        $this->arcManager = $arcManager;
    }

    /**
     * search one arc (only Ajax).
     *
     * @Route("/arc/{arcId}", name="odl_search_arc",
     * methods={"GET"},
     * condition="request.isXmlHttpRequest()",
     * options = { "expose" = true })
     */
    public function searchArc(string $arcId)
    {
        $package = new Package(new EmptyVersionStrategy());
        $arc = $this->arcManager->find($arcId);
        $period = $arc->getPeriod();
        $era = $period->getEra();
        $coverSource = $package->getUrl('/assets/img/covers/'.$arc->getCover());

        $twigArgs = [
            'arc' => $arc,
            'era' => $era,
            'coverSource' => $coverSource,
        ];

        $response = $this->renderView('partial/arc_line.html.twig', $twigArgs);

        return new JsonResponse(['html' => $response, 'title' => $period->getName()]);
    }

    /** search arcs by query.
     *
     * @Route("/arcs", name="odl_search_arcs",
     * options = { "expose" = true })
     */
    public function searchArcs(
        Request $request,
        EraManager $eraManager
    ) {
        $query = $request->get('q');
        $from = $request->get('from');

        if ('all' != $from) {
            $era = $eraManager->find($from);
        } else {
            $era = null;
        }

        $arcs = $this->arcManager->findArcsByQuery($query, $era);

        $twigArgs = [
            'arcs' => $arcs,
            'era' => $era,
            'query' => $query,
            'from' => $from,
        ];

        return $this->render('search.html.twig', $twigArgs);
    }
}
