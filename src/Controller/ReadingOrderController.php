<?php

namespace ODL\Controller;

use ODL\Service\EraManager;
use ODL\Service\UniverseManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */
class ReadingOrderController extends AbstractController
{
    private $universeManager;
    private $eraManager;

    public function __construct(
        UniverseManager $universeManager,
        EraManager $eraManager
    ) {
        $this->universeManager = $universeManager;
        $this->eraManager = $eraManager;
    }

    /**
     * Home page.
     *
     * @Route("/", name="odl_home_page")
     */
    public function home()
    {
        $universes = $this->universeManager->findAll()->getUniverses();

        return $this->render('home.html.twig', ['universes' => $universes]);
    }

    /**
     * Reading order page.
     *
     * @Route("/reading-order/{era_id}", name="odl_reading_order_page", methods={"GET"})
     */
    public function reading_order_page(string $era_id)
    {
        $era = $this->eraManager->findOne($era_id);
        $universe = $this->universeManager->findBy(['id' => $era->getUniverse()])[0];

        $twig_args = [
            'universe' => $universe,
            'era' => $era,
            'backButton' => $this->generateUrl('odl_home_page'),
        ];

        return $this->render('reading_order.html.twig', $twig_args);
    }
}
