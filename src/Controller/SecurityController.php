<?php

namespace ODL\Controller;

use Doctrine\ORM\EntityManagerInterface;
use ODL\Entity\User;
use ODL\Form\ForgottenPasswordFormType;
use ODL\Form\UserPasswordFormType;
use ODL\Service\ForgottenPasswordManager;
use ODL\Service\MailerManager;
use ODL\Service\UserManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * @Route("/security")
 */
class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="odl_security_login",
     * options = { "expose" = true })
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('odl_home_page');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="odl_security_logout")
     */
    public function logout()
    {
        throw new \Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
    }

    /**
     * @Route("/forgotten-password", name="odl_security_forgotten_password")
     */
    public function forgottenPassword(
        Request $request,
        UserManager $userManager,
        ForgottenPasswordManager $forgottenPasswordManager,
        MailerManager $mailer
    ) {
        $form = $this->createForm(ForgottenPasswordFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $emailResquested = $form->get('email')->getData();

            $user = $userManager->findOneBy(['email' => $emailResquested]);
            if ($user instanceof User) {
                $token = $forgottenPasswordManager->getForgottenPasswordToken($user);

                $mailTwigArgs = [
                    'user' => $user,
                    'url' => $this->generateUrl('odl_reset_password', ['token' => $token], UrlGeneratorInterface::ABSOLUTE_URL),
                ];

                $mailer->sendForgottenPasswordEmail($emailResquested, $mailTwigArgs);

                $this->addFlash(
                    'info',
                    'Un e-mail vous a été envoyé et restera valide 10min.'
                );
            } else {
                $this->addFlash(
                    'warning',
                    'Aucun compte n\'a été trouvé avec cet e-mail.'
                );
            }
        }

        return $this->render('security/forgotten-password.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/reset-password", name="odl_reset_password",
     * options = { "expose" = true })
     */
    public function resetPassword(
        Request $request,
        UserManager $userManager,
        ForgottenPasswordManager $forgottenPasswordManager,
        UserPasswordEncoderInterface $passwordEncoder,
        EntityManagerInterface $em
    ) {
        $twigArgs = [];
        $token = $request->get('token');
        $user = $userManager->findOneBy(['forgottenPassword' => $token]);

        if (!$token) {
            throw new NotFoundHttpException(); // todo
        }

        if ($user) {
            $form = $this->createForm(UserPasswordFormType::class, $user);
            $form->handleRequest($request);

            if ($form->isSubmitted()) {
                $newPassword = $form->get('plainNewPassword')->getData();
                $newPasswordRetry = $form->get('plainNewPasswordRetry')->getData();

                if ($newPassword == $newPasswordRetry) {
                    $user->setPassword(
                        $passwordEncoder->encodePassword($user, $newPassword)
                    );

                    $em->persist($user);
                    $em->flush();

                    $this->addFlash(
                        'info',
                        'Vos informations ont été mis à jour avec succès.'
                    );

                    $twigArgs = [
                        'redirection' => 'odl_home_page',
                    ];
                } else {
                    $this->addFlash(
                        'error',
                        'Les informations saisies sont erronées.'
                    );

                    $twigArgs = [
                        'form' => $form->createView(),
                    ];
                }
            } else {
                $dateString = explode('date', $token)[1] ?? null;

                if ($dateString && $forgottenPasswordManager->isValidDate($dateString)) {
                    $form->remove('plainActualPassword');
                    $twigArgs = [
                        'form' => $form->createView(),
                    ];
                } else {
                    $twigArgs = [
                        'message' => [
                            'type' => 'error',
                            'content' => 'Ce lien n\'est plus valide.',
                        ],
                    ];
                }
            }
        } else {
            $twigArgs = [
                'message' => [
                    'type' => 'error',
                    'content' => 'Ce lien n\'est pas valide.',
                ],
            ];
        }

        return $this->render('security/reset-password.html.twig', $twigArgs);
    }
}
