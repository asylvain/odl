<?php

namespace ODL\Controller;

use Doctrine\ORM\EntityManagerInterface;
use ODL\Form\ArcFormType;
use ODL\Form\ErasFormType;
use ODL\Form\MaintenanceFormType;
use ODL\Form\PeriodsFormType;
use ODL\Form\SettingsFormType;
use ODL\Form\UniversesFormType;
use ODL\Form\UserDataFormType;
use ODL\Form\UserPasswordFormType;
use ODL\Service\ArcManager;
use ODL\Service\ChangelogManager;
use ODL\Service\EraManager;
use ODL\Service\FileManager;
use ODL\Service\MaintenanceManager;
use ODL\Service\PeriodManager;
use ODL\Service\SettingManager;
use ODL\Service\UniverseManager;
use ODL\Service\UserManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/admin")
 */
class AdminController extends AbstractController
{
    private $request;
    private $universeManager;
    private $eraManager;

    public function __construct(
        RequestStack $request,
        UniverseManager $universeManager,
        EraManager $eraManager
    ) {
        $this->request = $request->getCurrentRequest();
        $this->universeManager = $universeManager;
        $this->eraManager = $eraManager;
    }

    /**
     * @Route("/", name="odl_admin_home")
     */
    public function index()
    {
        $twig_args = [
            'backButton' => $this->generateUrl('odl_home_page'),
        ];

        return $this->render('admin/home.html.twig', $twig_args);
    }

    /**
     * @Route("/journal", name="odl_admin_journal")
     */
    public function journal(ChangelogManager $changelog)
    {
        $journal = $changelog->findAll();

        $twig_args = [
            'journal' => $journal,
            'backButton' => $this->generateUrl('odl_admin_home'),
        ];

        return $this->render('admin/journal.html.twig', $twig_args);
    }

    /**
     * @Route("/manage-universe", name="odl_admin_manage_universe")
     * @IsGranted("ROLE_ADMIN")
     */
    public function manageUniverses()
    {
        $twig_args = [];
        $universes = $this->universeManager->findAll();
        $form = $this->createForm(UniversesFormType::class, $universes);
        $form->handleRequest($this->request);

        // Construct universe form
        if (!$form->isSubmitted()) {
            $twig_args = [
                'form' => $form->createView(),
                'formType' => 'universes',
                'entityType' => 'univers',
                'entityTypeClean' => 'universe',
                'childEntityType' => 'era',
                'backButton' => $this->generateUrl('odl_admin_home'),
            ];

            $twig_args = array_merge($twig_args);

            return $this->render('admin/manage_sections.html.twig', $twig_args);
        } else {
            $messageFlash = false;
            $updated = false;
            $entityManager = $this->getDoctrine()->getManager();

            $dataSubmitted = $form->getData();

            // Update universe names
            $updated = $this->universeManager->upsertUniverse($dataSubmitted);

            if ($updated) {
                $entityManager->flush();
            }

            if (!$messageFlash && !$updated) {
                $messageFlash = 'Aucune modification.';
            } elseif (!$messageFlash && $updated) {
                $messageFlash = 'Vos modifications ont bien été prises en compte.';
            }

            $this->addFlash(
                'success',
                $messageFlash
            );

            return $this->redirectToRoute('odl_admin_manage_universe');
        }
    }

    /**
     * @Route("/manage-era/{id}", name="odl_admin_manage_era",
     * options = { "expose" = true })
     * @IsGranted("ROLE_ADMIN")
     */
    public function manageEras(string $id = '')
    {
        $universe_id = $id;
        $twig_args = [];
        $eras = $this->eraManager->findBy(['universe' => $universe_id]);
        $form = $this->createForm(ErasFormType::class, $eras);
        $form->handleRequest($this->request);
        $universe = $this->universeManager->findOne($universe_id);

        // Construct era form
        if (!$form->isSubmitted()) {
            $twig_args = [
                'form' => $form->createView(),
                'formType' => 'eras',
                'entityType' => 'ère',
                'entityTypeClean' => 'era',
                'parentEntityType' => 'universe',
                'parentEntity' => $universe,
                'childEntityType' => 'period',
                'backButton' => $this->generateUrl('odl_admin_manage_universe'),
            ];

            $twig_args = array_merge($twig_args);

            return $this->render('admin/manage_sections.html.twig', $twig_args);
        } else {
            $dataSubmitted = $form->getData();
            $imageSubmitted = $this->request->files->get('eras_form')['eras'];

            // Update era names
            $upserted = $this->eraManager->upsertEra($dataSubmitted, $imageSubmitted, $universe);

            $this->addFlash(
                $upserted['type'],
                $upserted['message']
            );

            return $this->redirectToRoute('odl_admin_manage_era', ['id' => $universe_id]);
        }
    }

    /**
     * @Route("/manage-period/{id}", name="odl_admin_manage_period",
     * options = { "expose" = true })
     * @IsGranted("ROLE_ADMIN")
     */
    public function managePeriods(string $id = '', PeriodManager $periodManager)
    {
        $era_id = $id;
        $twig_args = [];
        $periods = $periodManager->findBy(['era' => $id]);
        $form = $this->createForm(PeriodsFormType::class, $periods);
        $form->handleRequest($this->request);

        $era = $this->eraManager->find($era_id);

        // Construct period form
        if (!$form->isSubmitted()) {
            $twig_args = [
                'form' => $form->createView(),
                'formType' => 'periods',
                'entityType' => 'période',
                'entityTypeClean' => 'period',
                'parentEntityType' => 'era',
                'parentEntity' => $era,
                'backButton' => $this->generateUrl('odl_admin_manage_era', ['id' => $era->getUniverse()->getId()]),
            ];

            return $this->render('admin/manage_sections.html.twig', $twig_args);
        } else {
            $dataSubmitted = $form->getData();

            // Update period names
            $upserted = $periodManager->upsertPeriod($dataSubmitted, $era);

            $this->addFlash(
                $upserted['type'],
                $upserted['message']
            );

            return $this->redirectToRoute('odl_admin_manage_period', ['id' => $era_id]);
        }
    }

    /**
     * @Route("/create-arc", name="odl_admin_add_arc")
     */
    public function createArc(ArcManager $arcManager)
    {
        $twig_args = [];
        $form = $this->createForm(ArcFormType::class);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $arcSubmitted = $form->getData();
            $coverSubmitted = $this->request->files->get('arc_form')['cover'];

            $created = $arcManager->createArc($arcSubmitted, $coverSubmitted);

            $this->addFlash(
                $created['type'],
                $created['message']
            );
        }

        $twig_args = [
            'form' => $form->createView(),
            'backButton' => $this->generateUrl('odl_admin_home'),
        ];

        return $this->render('admin/add_arc.html.twig', $twig_args);
    }

    /**
     * @Route("/settings", name="odl_admin_settings")
     * @IsGranted("ROLE_SUPER_ADMIN")
     */
    public function settings(SettingManager $settingManager, MaintenanceManager $maintenanceManager)
    {
        $twig_args = [];
        $settings = $settingManager->findAll();
        $formMaintenance = $this->createForm(MaintenanceFormType::class);
        $formMaintenance->handleRequest($this->request);
        $form = $this->createForm(SettingsFormType::class);
        $form->handleRequest($this->request);

        if ($formMaintenance->isSubmitted() && $formMaintenance->isValid()) {
            $maintenance = $formMaintenance->getData();
            $message = $maintenanceManager->handleMaintenance($maintenance['active'], $maintenance['ttl']);

            $this->addFlash(
                'info',
                $message
            );
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $dataSubmitted = $form->getData();
            $updated = $settingManager->update($dataSubmitted);

            $this->addFlash(
                $updated['type'],
                $updated['message']
            );
        }

        $twig_args = [
            'settings' => $settings,
            'form' => $form->createView(),
            'formMaintenance' => $formMaintenance->createView(),
            'backButton' => $this->generateUrl('odl_admin_home'),
        ];

        return $this->render('admin/settings.html.twig', $twig_args);
    }

    /**
     * @Route("/profile", name="odl_admin_profile")
     * @Route("/edit-user/{userId}", name="odl_admin_edit_user")
     */
    public function profile(
        string $userId = '',
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $passwordEncoder,
        UserManager $userManager,
        FileManager $fileManager
    ) {
        $twig_args = [];

        if (empty($userId)) {
            $user = $this->getUser();
            $backRouteName = 'odl_admin_home';
        } else {
            $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN');
            $user = $userManager->find($userId);
            $backRouteName = 'odl_admin_users';
        }

        $formUserData = $this->createForm(UserDataFormType::class, $user);
        $formUserData->handleRequest($this->request);
        $formUserPassword = $this->createForm(UserPasswordFormType::class, $user);
        $formUserPassword->handleRequest($this->request);

        if ($formUserPassword->isSubmitted() && $formUserPassword->isValid()) {
            $newPassword = $formUserPassword->get('plainNewPassword')->getData();
            $newPasswordRetry = $formUserPassword->get('plainNewPasswordRetry')->getData();
            $isOlderPasswordValid = $passwordEncoder->isPasswordValid($user, $formUserPassword->get('plainActualPassword')->getData());

            if ($isOlderPasswordValid && ($newPassword == $newPasswordRetry)) {
                $user->setPassword(
                    $passwordEncoder->encodePassword($user, $newPassword)
                );

                $em->persist($user);
                $em->flush();

                $this->addFlash(
                    'info',
                    'Vos informations ont été mis à jour avec succès.'
                );
            } else {
                $this->addFlash(
                    'error',
                    'Les informations saisies sont erronées.'
                );
            }
        } elseif ($formUserData->isSubmitted() && $formUserData->isValid()) {
            $avatar64 = $formUserData->get('avatar_base64')->getData() ?: '';
            $updated = $userManager->update($user, $avatar64, $fileManager);

            $this->addFlash(
                $updated['type'],
                $updated['message']
            );
        }

        $twig_args = [
            'user' => $user,
            'formUserData' => $formUserData->createView(),
            'formUserPassword' => $formUserPassword->createView(),
            'backButton' => $this->generateUrl($backRouteName),
        ];

        return $this->render('admin/profile.html.twig', $twig_args);
    }

    /**
     * @Route("/users", name="odl_admin_users")
     * @IsGranted("ROLE_SUPER_ADMIN")
     */
    public function users(UserManager $userManager)
    {
        $users = $userManager->findAll();

        $twig_args = [
            'users' => $users,
            'backButton' => $this->generateUrl('odl_admin_home'),
        ];

        return $this->render('admin/users.html.twig', $twig_args);
    }
}
