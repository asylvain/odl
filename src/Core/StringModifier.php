<?php

namespace ODL\Core;

class StringModifier
{
    public function cleanNameGenerator(string $name): string
    {
        $search = [' ', '(', ')', '/', '\\'];
        $replace = '_';
        $cleanName = strtolower(str_replace($search, $replace, $name));

        return $cleanName;
    }

    public function image_name_generator(): string
    {
        return md5(uniqid(rand(), true));
    }
}
