<?php

namespace ODL\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class CustomExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        $ownClasses = get_class_methods($this);
        $classesToLoad = [];

        if (!empty($ownClasses)) {
            foreach ($ownClasses as $ownClass) {
                $classesToLoad[] = new TwigFilter($ownClass, [$this, $ownClass]);
            }
        }

        return $classesToLoad;
    }

    public function unescape(string $value): string
    {
        return html_entity_decode($value, ENT_QUOTES);
    }

    public function bool(?string $value): bool
    {
        return (bool) $value;
    }
}
