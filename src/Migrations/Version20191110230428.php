<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191110230428 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE setting (id INT AUTO_INCREMENT NOT NULL, param VARCHAR(60) NOT NULL, value LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE changelog (id INT AUTO_INCREMENT NOT NULL, author LONGTEXT NOT NULL, cl_type LONGTEXT NOT NULL, name_universe LONGTEXT NOT NULL, name_era LONGTEXT NOT NULL, name_period LONGTEXT NOT NULL, old_position LONGTEXT NOT NULL, new_position LONGTEXT NOT NULL, title LONGTEXT NOT NULL, new_title LONGTEXT NOT NULL, cover LONGTEXT NOT NULL, content LONGTEXT NOT NULL, link_a LONGTEXT NOT NULL, link_b LONGTEXT NOT NULL, is_event LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE setting');
        $this->addSql('DROP TABLE changelog');
    }
}
