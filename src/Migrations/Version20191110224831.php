<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191110224831 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE period (id VARCHAR(13) NOT NULL, era_id VARCHAR(13) NOT NULL, position INT NOT NULL, name VARCHAR(100) NOT NULL, clean_name VARCHAR(100) NOT NULL, INDEX IDX_C5B81ECE707300A1 (era_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE era (id VARCHAR(13) NOT NULL, universe_id VARCHAR(13) NOT NULL, position INT NOT NULL, name VARCHAR(100) NOT NULL, clean_name VARCHAR(100) NOT NULL, image LONGTEXT NOT NULL, INDEX IDX_96E19A635CD9AF2 (universe_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE arc (id INT AUTO_INCREMENT NOT NULL, period_id VARCHAR(13) NOT NULL, title LONGTEXT NOT NULL, cover LONGTEXT NOT NULL, content LONGTEXT NOT NULL, link_a LONGTEXT NOT NULL, link_b LONGTEXT NOT NULL, is_event TINYINT(1) NOT NULL, INDEX IDX_7FE65393EC8B7ADE (period_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE universe (id VARCHAR(13) NOT NULL, name VARCHAR(50) NOT NULL, clean_name VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE period ADD CONSTRAINT FK_C5B81ECE707300A1 FOREIGN KEY (era_id) REFERENCES era (id)');
        $this->addSql('ALTER TABLE era ADD CONSTRAINT FK_96E19A635CD9AF2 FOREIGN KEY (universe_id) REFERENCES universe (id)');
        $this->addSql('ALTER TABLE arc ADD CONSTRAINT FK_7FE65393EC8B7ADE FOREIGN KEY (period_id) REFERENCES period (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE arc DROP FOREIGN KEY FK_7FE65393EC8B7ADE');
        $this->addSql('ALTER TABLE period DROP FOREIGN KEY FK_C5B81ECE707300A1');
        $this->addSql('ALTER TABLE era DROP FOREIGN KEY FK_96E19A635CD9AF2');
        $this->addSql('DROP TABLE period');
        $this->addSql('DROP TABLE era');
        $this->addSql('DROP TABLE arc');
        $this->addSql('DROP TABLE universe');
    }
}
