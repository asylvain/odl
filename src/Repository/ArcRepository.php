<?php

namespace ODL\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use ODL\Entity\Arc;

/**
 * @method Arc|null find($id, $lockMode = null, $lockVersion = null)
 * @method Arc|null findOneBy(array $criteria, array $orderBy = null)
 * @method Arc[]    findAll()
 * @method Arc[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArcRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Arc::class);
    }

    public function getCovers(array $ids): array
    {
        return $this->createQueryBuilder('a')
            ->select('a.cover')
            ->where('a.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->orderBy('a.position', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findLastArc(string $periodId)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.period = :period_id')
            ->setParameter('period_id', $periodId)
            ->orderBy('p.position', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
    }

    public function findArcsFromPeriodBetweenPositions(string $periodId, $first, $last): array
    {
        return $this->createQueryBuilder('p')
            ->where('p.period = :period_id')
            ->andWhere('p.position BETWEEN :first AND :last')
            ->setParameter('period_id', $periodId)
            ->setParameter('first', $first)
            ->setParameter('last', $last)
            ->orderBy('p.position', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findArcsByQuery(string $query, array $periodIds = []): array
    {
        $q = $this->createQueryBuilder('a')
            ->where('a.title LIKE :query')
            ->orWhere('a.content LIKE :query')
            ->setParameter('query', '%'.$query.'%');

        if (!empty($periodIds)) {
            $q->andWhere('a.period IN (:periodIds)')
                ->setParameter('periodIds', $periodIds);
        }

        return $q->getQuery()
            ->getResult();
    }
}
