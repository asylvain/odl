# ODL

[![lang: PHP 7.1](https://img.shields.io/badge/Lang-PHP%207.1-brightgreen.svg)](http://php.net/manual/migration71.php)
[![framework: Symfony4.3](https://img.shields.io/badge/Symfony-4.3-blue)](https://symfony.com/doc/4.3/index.html#gsc.tab=0)

[![pipeline status](https://gitlab.com/adynemo/odl/badges/master/pipeline.svg)](https://gitlab.com/adynemo/odl/-/commits/master)

## Description

ODL for Ordre De Lecture in french, reading order in english.

This is a CMS to create a website for several reading orders (comics, books, TV show,...).
